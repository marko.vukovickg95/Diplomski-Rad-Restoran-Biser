﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class ReservationBusiness : IReservationBusiness
    {

        // POVEZIVANJE SA DATA LAYER-OM
        private IReservationRepository reservationRepository;


        public ReservationBusiness(IReservationRepository reservationRepository)
        {
            this.reservationRepository = reservationRepository;
        }

        //LOGICKA PROVERA ZA UNOS GLUMCA
        public bool InsertReservation(Reservation r)
        {
            if (reservationRepository.InsertReservation(r) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //LOGICKA PROVERA ZA ISPIS GLUMCA
        public List<Reservation> SelectAllReservations()
        {
            return reservationRepository.SelectAllReservations();

        }

        ////LOGICKA PROVERA ZA AZURIRANJE GLUMCA
        public bool UpdateReservation(Reservation r)
        {
            if (reservationRepository.UpdateReservation(r) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
