﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class BillBusiness : IBillBusiness
    {
        // POVEZIVANJE SA DATA LAYER-OM
        private IBillRepository billRepository;


        public BillBusiness(IBillRepository billRepository)
        {
            this.billRepository = billRepository;
        }

        //LOGICKA PROVERA ZA UNOS GLUMCA
        public bool InsertBill(Bill b)
        {
            if (billRepository.InsertBill(b) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //LOGICKA PROVERA ZA ISPIS GLUMCA
        public List<Bill> SelectAllBills()
        {
            return billRepository.SelectAllBills();

        }

        ////LOGICKA PROVERA ZA AZURIRANJE GLUMCA
        public bool UpdateBill(Bill b)
        {
            if (billRepository.UpdateBill(b) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

      
    }
}
