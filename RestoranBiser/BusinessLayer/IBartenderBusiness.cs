﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
   public interface IBartenderBusiness
    {
        bool InsertBartender(Bartender b);
        List<Bartender> SelectAllBartenders();
        bool UpdateBartender(Bartender b);
        bool DeleteBartender(Bartender b);
        List<Bartender> Login(string username, string password);
        List<Bartender> SelectBartenderById(string id);
    }
}
