﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public interface IMenu_ItemBusiness
    {
        bool InsertMenu_Item(Menu_Item m);
        List<Menu_Item> SelectAllMenu_Items();
        bool UpdateMenu_Item(Menu_Item m);
        bool DeleteMenu_Item(Menu_Item m);
    }
}
