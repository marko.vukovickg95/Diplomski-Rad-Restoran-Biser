﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class SpotBusiness : ISpotBusiness
    {
        // POVEZIVANJE SA DATA LAYER-OM
        private ISpotRepository spotRepository;


        public SpotBusiness(ISpotRepository spotRepository)
        {
            this.spotRepository = spotRepository;
        }

        //LOGICKA PROVERA ZA UNOS GLUMCA
        public bool InsertSpot(Spot s)
        {
            if (spotRepository.InsertSpot(s) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //LOGICKA PROVERA ZA ISPIS GLUMCA
        public List<Spot> SelectAllSpots()
        {
            return spotRepository.SelectAllSpots();

        }

        ////LOGICKA PROVERA ZA AZURIRANJE GLUMCA
        public bool UpdateSpot(Spot s)
        {
            if (spotRepository.UpdateSpot(s) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        ////LOGICKA PROVERA ZA BRISANJE GLUMCA
        public bool DeleteSpot(Spot s)
        {
            if (spotRepository.DeleteSpot(s) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
