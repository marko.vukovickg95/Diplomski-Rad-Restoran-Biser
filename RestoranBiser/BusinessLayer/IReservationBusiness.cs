﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public interface IReservationBusiness
    {
        bool InsertReservation(Reservation r);
        List<Reservation> SelectAllReservations();
        bool UpdateReservation(Reservation r);
    }
}
