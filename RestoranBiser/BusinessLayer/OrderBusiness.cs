﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
  public class OrderBusiness
    {

        // POVEZIVANJE SA DATA LAYER-OM
        private IOrderRepository orderRepository;


        public OrderBusiness(IOrderRepository orderRepository)
        {
            this.orderRepository = orderRepository;
        }

        //LOGICKA PROVERA ZA UNOS GLUMCA
        public bool InsertOrder(Order o)
        {
            if (orderRepository.InsertOrder(o) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //LOGICKA PROVERA ZA ISPIS GLUMCA
        public List<Order> SelectAllOrders()
        {
            return orderRepository.SelectAllOrders();

        }
    }
}
