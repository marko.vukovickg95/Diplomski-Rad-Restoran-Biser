﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class OfferBusiness : IOfferBusiness
    {
        // POVEZIVANJE SA DATA LAYER-OM
        private IOfferRepository offerRepository;


        public OfferBusiness(IOfferRepository offerRepository)
        {
            this.offerRepository = offerRepository;
        }

        //LOGICKA PROVERA ZA UNOS PONUDE
        public bool InsertOffer(Offer o)
        {
            if (offerRepository.InsertOffer(o) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //LOGICKA PROVERA ZA ISPIS PONUDE
        public List<Offer> SelectAllOffers()
        {
            return offerRepository.SelectAllOffers();

        }

        ////LOGICKA PROVERA ZA AZURIRANJE PONUDE
        public bool UpdateOffer(Offer o)
        {
            if (offerRepository.UpdateOffer(o) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        ////LOGICKA PROVERA ZA BRISANJE PONUDE
        public bool DeleteOffer(Offer o)
        {
            if (offerRepository.DeleteOffer(o) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
