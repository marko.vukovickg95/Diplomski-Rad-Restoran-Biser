﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public interface IOfferBusiness
    {
        bool InsertOffer(Offer o);
        List<Offer> SelectAllOffers();
        bool UpdateOffer(Offer o);
        bool DeleteOffer(Offer o);
    }
}
