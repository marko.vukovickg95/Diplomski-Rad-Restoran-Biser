﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public interface ISpotBusiness
    {
        bool InsertSpot(Spot s);
        List<Spot> SelectAllSpots();
        bool UpdateSpot(Spot s);
        bool DeleteSpot(Spot s);
    }
}
