﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class BartenderBusiness : IBartenderBusiness
    {
        // POVEZIVANJE SA DATA LAYER-OM
        private IBartenderRepository bartenderRepository;


        public BartenderBusiness(IBartenderRepository bartenderRepository)
        {
            this.bartenderRepository = bartenderRepository;
        }

        //LOGICKA PROVERA ZA UNOS GLUMCA
        public bool InsertBartender(Bartender b)
        {
            if (bartenderRepository.InsertBartender(b) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //LOGICKA PROVERA ZA ISPIS GLUMCA
        public List<Bartender> SelectAllBartenders()
        {
            return bartenderRepository.SelectAllBartenders();

        }

        //ZA LOGIN
        public List<Bartender> Login(string username, string password)
        {
            return bartenderRepository.SelectAllBartenders().Where(b => b.Username1.Equals(username) && b.Password1.Equals(password)).ToList();

        }

        //ZA MENI
        public List<Bartender> SelectBartenderById(string pom)
        {
            int id = Int32.Parse(pom);
            return bartenderRepository.SelectAllBartenders().Where(b => b.Bartender_Id1 == id).ToList();

        }

        ////LOGICKA PROVERA ZA AZURIRANJE GLUMCA
        public bool UpdateBartender(Bartender b)
        {
            if (bartenderRepository.UpdateBartender(b) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        ////LOGICKA PROVERA ZA BRISANJE GLUMCA
        public bool DeleteBartender(Bartender b)
        {
            if (bartenderRepository.DeleteBartender(b) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
