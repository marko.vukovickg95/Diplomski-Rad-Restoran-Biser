﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class Menu_ItemBusiness : IMenu_ItemBusiness
    {

        // POVEZIVANJE SA DATA LAYER-OM
        private IMenu_ItemRepository menu_ItemRepository;


        public Menu_ItemBusiness(IMenu_ItemRepository menu_ItemRepository)
        {
            this.menu_ItemRepository = menu_ItemRepository;
        }

        //LOGICKA PROVERA ZA UNOS GLUMCA
        public bool InsertMenu_Item(Menu_Item m)
        {
            if (menu_ItemRepository.InsertMenu_Item(m) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //LOGICKA PROVERA ZA ISPIS GLUMCA
        public List<Menu_Item> SelectAllMenu_Items()
        {
            return menu_ItemRepository.SelectAllMenu_Items();

        }

        ////LOGICKA PROVERA ZA AZURIRANJE GLUMCA
        public bool UpdateMenu_Item(Menu_Item m)
        {
            if (menu_ItemRepository.UpdateMenu_Item(m) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        ////LOGICKA PROVERA ZA BRISANJE GLUMCA
        public bool DeleteMenu_Item(Menu_Item m)
        {
            if (menu_ItemRepository.DeleteMenu_Item(m) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
