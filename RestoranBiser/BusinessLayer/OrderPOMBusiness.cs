﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
  public  class OrderPOMBusiness
    {
        // POVEZIVANJE SA DATA LAYER-OM
        private IOrderPOMRepository orderPOMRepository;


        public OrderPOMBusiness(IOrderPOMRepository orderPOMRepository)
        {
            this.orderPOMRepository = orderPOMRepository;
        }

        //LOGICKA PROVERA ZA UNOS GLUMCA
        public bool InsertOrderPOM(OrderPOM o)
        {
            if (orderPOMRepository.InsertOrderPOM(o) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //LOGICKA PROVERA ZA ISPIS GLUMCA
        public List<OrderPOM> SelectAllOrderPOMs()
        {
            return orderPOMRepository.SelectAllPOMOrders();

        }

        public bool DeleteOrderPOM()
        {
            if (orderPOMRepository.DeletePOMOrders() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
