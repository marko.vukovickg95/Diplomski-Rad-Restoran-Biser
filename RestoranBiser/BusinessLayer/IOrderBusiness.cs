﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
   public interface IOrderBusiness
    {

        bool InsertOrder(Order o);
        List<Order> SelectAllOrders();
    }
}
