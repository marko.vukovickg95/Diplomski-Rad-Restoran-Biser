﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
   public interface IBillBusiness
    {
        bool InsertBill(Bill b);
        List<Bill> SelectAllBills();
        bool UpdateBill(Bill b);
    }
}
