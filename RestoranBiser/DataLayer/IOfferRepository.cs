﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
   public interface IOfferRepository
    {
        int InsertOffer(Offer o);
        List<Offer> SelectAllOffers();
        int UpdateOffer(Offer o);
        int DeleteOffer(Offer o);
    }
}
