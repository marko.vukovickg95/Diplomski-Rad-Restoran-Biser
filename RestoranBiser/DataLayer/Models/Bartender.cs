﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class Bartender
    {
        private int Bartender_Id;
        private string Name;
        private string Surname;
        private int JMBG;
        private int Card_Number;
        private int Phone_Number;
        private string Username;
        private string Password;

        public Bartender() { }

        public Bartender(int bartender_Id, string name, string surname, int jMBG, int card_Number, int phone_Number, string username, string password)
        {
            Bartender_Id = bartender_Id;
            Name = name;
            Surname = surname;
            JMBG = jMBG;
            Card_Number = card_Number;
            Phone_Number = phone_Number;
            Username = username;
            Password = password;
        }

        public int Bartender_Id1 { get => Bartender_Id; set => Bartender_Id = value; }
        public string Name1 { get => Name; set => Name = value; }
        public string Surname1 { get => Surname; set => Surname = value; }
        public int JMBG1 { get => JMBG; set => JMBG = value; }
        public int Card_Number1 { get => Card_Number; set => Card_Number = value; }
        public int Phone_Number1 { get => Phone_Number; set => Phone_Number = value; }
        public string Username1 { get => Username; set => Username = value; }
        public string Password1 { get => Password; set => Password = value; }
    }
}
