﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class Reservation
    {
        private int Reservation_Id;
        private string Event;
        private DateTime Date_Of_Event;
        private string Time_Of_Event;
        private int Number_Of_Guests;
        private int Cost;
        private string Customer;
        private int Phone_Number;
        private string Status;
        private DateTime Date_Of_Payment;
        private int Offer_Id;
        private int Bartender_Id;

        public Reservation() { }

        public Reservation(int reservation_Id, string @event, DateTime date_Of_Event, string time_of_event, int number_Of_Guests, int cost, string customer, int phone_Number, string status, DateTime date_Of_Payment, int offer_Id, int bartender_Id)
        {
            Reservation_Id = reservation_Id;
            Event = @event;
            Date_Of_Event = date_Of_Event;
            Time_Of_Event = time_of_event;
            Number_Of_Guests = number_Of_Guests;
            Cost = cost;
            Customer = customer;
            Phone_Number = phone_Number;
            Status = status;
            Date_Of_Payment = date_Of_Payment;
            Offer_Id = offer_Id;
            Bartender_Id = bartender_Id;
        }

        public int Reservation_Id1 { get => Reservation_Id; set => Reservation_Id = value; }
        public string Event1 { get => Event; set => Event = value; }
        public DateTime Date_Of_Event1 { get => Date_Of_Event; set => Date_Of_Event = value; }
        public string Time_Of_Event1 { get => Time_Of_Event; set => Time_Of_Event = value; }
        public int Number_Of_Guests1 { get => Number_Of_Guests; set => Number_Of_Guests = value; }
        public int Cost1 { get => Cost; set => Cost = value; }
        public string Customer1 { get => Customer; set => Customer = value; }
        public int Phone_Number1 { get => Phone_Number; set => Phone_Number = value; }
        public string Status1 { get => Status; set => Status = value; }
        public DateTime Date_Of_Payment1 { get => Date_Of_Payment; set => Date_Of_Payment = value; }
        public int Offer_Id1 { get => Offer_Id; set => Offer_Id = value; }
        public int Bartender_Id1 { get => Bartender_Id; set => Bartender_Id = value; }
    }
}
