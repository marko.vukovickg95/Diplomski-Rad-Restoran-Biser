﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
   public class Bill
    {
        private int Bill_Id;
        private DateTime Date_Of_Bill;
        private int Cost;
        private string Status;
        private int Bartender_Id;
        private int Spot_Id;

        public Bill() { }

        public Bill(int bill_Id, DateTime date_Of_Bill, int cost, string status, int bartender_Id, int spot_Id)
        {
            Bill_Id = bill_Id;
            Date_Of_Bill = date_Of_Bill;
            Cost = cost;
            Status = status;
            Bartender_Id = bartender_Id;
            Spot_Id = spot_Id;
        }

        public int Bill_Id1 { get => Bill_Id; set => Bill_Id = value; }
        public DateTime Date_Of_Bill1 { get => Date_Of_Bill; set => Date_Of_Bill = value; }
        public int Cost1 { get => Cost; set => Cost = value; }
        public string Status1 { get => Status; set => Status = value; }
        public int Bartender_Id1 { get => Bartender_Id; set => Bartender_Id = value; }
        public int Spot_Id1 { get => Spot_Id; set => Spot_Id = value; }
    }
}
