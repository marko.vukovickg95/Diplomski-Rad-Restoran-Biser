﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
   public class Spot
    {
        private int Spot_Id;
        private string Status;

        public Spot() { }

        public Spot(int spot_Id, string status)
        {
            Spot_Id = spot_Id;
            Status = status;
        }

        public int Spot_Id1 { get => Spot_Id; set => Spot_Id = value; }
        public string Status1 { get => Status; set => Status = value; }
    }
}
