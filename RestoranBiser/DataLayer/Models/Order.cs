﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
   public class Order
    {
        private int Order_Id;
        private int Amount;
        private int Price;
        private int Menu_Item_Id;
        private int Bill_Id;

        public Order() { }

        public Order(int order_Id, int amount, int price, int menu_Item_Id, int bill_Id)
        {
            Order_Id = order_Id;
            Amount = amount;
            Price = price;
            Menu_Item_Id = menu_Item_Id;
            Bill_Id = bill_Id;
        }

        public int Order_Id1 { get => Order_Id; set => Order_Id = value; }
        public int Amount1 { get => Amount; set => Amount = value; }
        public int Price1 { get => Price; set => Price = value; }
        public int Menu_Item_Id1 { get => Menu_Item_Id; set => Menu_Item_Id = value; }
        public int Bill_Id1 { get => Bill_Id; set => Bill_Id = value; }
    }
}
