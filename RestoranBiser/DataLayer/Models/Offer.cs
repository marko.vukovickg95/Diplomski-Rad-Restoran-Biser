﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
   public class Offer
    {
        private int Offer_Id;
        private string Type;
        private int Price;
        private string Description;

        public Offer() { }

        public Offer(int offer_Id, string type, int price, string description)
        {
            Offer_Id = offer_Id;
            Type = type;
            Price = price;
            Description = description;
        }

        public int Offer_Id1 { get => Offer_Id; set => Offer_Id = value; }
        public string Type1 { get => Type; set => Type = value; }
        public int Price1 { get => Price; set => Price = value; }
        public string Description1 { get => Description; set => Description = value; }
    }
}
