﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
   public class Menu_Item
    {
        private int Menu_Item_Id;
        private string Item_Name;
        private int Price;
        private string Item_Category;
        private string Item_Sort;
        private string Description;

        public Menu_Item() { }

        public Menu_Item(int menu_Item_Id, string item_Name, int price, string item_Category, string item_Sort, string description)
        {
            Menu_Item_Id = menu_Item_Id;
            Item_Name = item_Name;
            Price = price;
            Item_Category = item_Category;
            Item_Sort = item_Sort;
            Description = description;
        }

        public int Menu_Item_Id1 { get => Menu_Item_Id; set => Menu_Item_Id = value; }
        public string Item_Name1 { get => Item_Name; set => Item_Name = value; }
        public int Price1 { get => Price; set => Price = value; }
        public string Item_Category1 { get => Item_Category; set => Item_Category = value; }
        public string Item_Sort1 { get => Item_Sort; set => Item_Sort = value; }
        public string Description1 { get => Description; set => Description = value; }
    }
}
