﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
   public class OrderPOM
    {
        private int OrderPOM_Id;
        private int Menu_Item_Id;
        private int Amount;
        private int Price;

        public OrderPOM() { }

        public OrderPOM(int orderPOM_Id, int menu_Item_Id, int amount, int price)
        {
            OrderPOM_Id = orderPOM_Id;
            Menu_Item_Id = menu_Item_Id;
            Amount = amount;
            Price = price;
        }

        public int OrderPOM_Id1 { get => OrderPOM_Id; set => OrderPOM_Id = value; }
        public int Menu_Item_Id1 { get => Menu_Item_Id; set => Menu_Item_Id = value; }
        public int Amount1 { get => Amount; set => Amount = value; }
        public int Price1 { get => Price; set => Price = value; }
    }

    
}
