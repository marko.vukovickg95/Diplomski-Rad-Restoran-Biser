﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public interface IBillRepository
    {
        int InsertBill(Bill b);
        List<Bill> SelectAllBills();
        int UpdateBill(Bill b);
    }
}
