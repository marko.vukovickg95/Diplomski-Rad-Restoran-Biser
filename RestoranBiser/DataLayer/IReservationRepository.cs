﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
   public interface IReservationRepository
    {
        int InsertReservation(Reservation r);
        List<Reservation> SelectAllReservations();
        int UpdateReservation(Reservation r);

    }
}
