﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
   public class Menu_ItemRepository : IMenu_ItemRepository
    {

        //KREIRANJE UPITA ZA UNOS PONUDE U BAZU!

        public int InsertMenu_Item(Menu_Item m)
        {
            SqlConnection dataConnection = new SqlConnection();

            dataConnection.ConnectionString = GlobalVariables.connString;
            dataConnection.Open();

            SqlCommand command = new SqlCommand(); // kreiranje komande
            command.Connection = dataConnection; //setovanje konekcije komande
            command.CommandText = "INSERT INTO Menu_Items VALUES('" + m.Item_Name1+ "', '" + m.Price1 + "', '" + m.Item_Category1 + "', '" + m.Item_Sort1 + "', '" + m.Description1 + "')"; // setovanje SQL upita koji će se izvršiti nad bazom podataka

            return command.ExecuteNonQuery();
        }

        //KREIRANJE UPITA ZA ISPIS PONUDE!!!

        public List<Menu_Item> SelectAllMenu_Items()
        {
            List<Menu_Item> lista = new List<Menu_Item>();

            SqlConnection dataConnection = new SqlConnection();

            dataConnection.ConnectionString = GlobalVariables.connString;
            dataConnection.Open();

            SqlCommand command = new SqlCommand(); // kreiranje komande
            command.Connection = dataConnection; //setovanje konekcije komande
            command.CommandText = "SELECT * FROM Menu_Items";

            SqlDataReader dataReader = command.ExecuteReader();

            while (dataReader.Read())
            {
                Menu_Item m = new Menu_Item();
                // za svaki red se uzima vrednost određene kolone (0 - prva kolona)
                m.Menu_Item_Id1 = dataReader.GetInt32(0);
                m.Item_Name1 = dataReader.GetString(1);
                m.Price1 = dataReader.GetInt32(2);
                m.Item_Category1 = dataReader.GetString(3);
                m.Item_Sort1 = dataReader.GetString(4);
                m.Description1 = dataReader.GetString(5);

                lista.Add(m); // svaki student se na kraju može ubaciti u neku listu
            }

            dataConnection.Close();
            return lista;
        }

        //UPDATE METODA ZA AZURIRANJE PONUDE
        public int UpdateMenu_Item(Menu_Item m)
        {
            SqlConnection dataConnection = new SqlConnection();

            dataConnection.ConnectionString = GlobalVariables.connString;
            dataConnection.Open();

            SqlCommand command = new SqlCommand(); // kreiranje komande
            command.Connection = dataConnection; //setovanje konekcije komande
            command.CommandText = "UPDATE Menu_Items SET Item_Name = '" + m.Item_Name1 + "', Price = '" + m.Price1 + "', Item_Category = '" + m.Item_Category1 + "', Item_Sort = '" + m.Item_Sort1 + "', Description = '" + m.Description1 + "' WHERE Menu_Item_Id = '" + m.Menu_Item_Id1 + "'"; // setovanje SQL upita koji će se izvršiti nad bazom podataka

            return command.ExecuteNonQuery();
            dataConnection.Close();
        }

        //DELETE METODA ZA BRISANJE PONUDE
        public int DeleteMenu_Item(Menu_Item m)
        {
            SqlConnection dataConnection = new SqlConnection();

            dataConnection.ConnectionString = GlobalVariables.connString;
            dataConnection.Open();

            SqlCommand command = new SqlCommand(); // kreiranje komande
            command.Connection = dataConnection; //setovanje konekcije komande
            command.CommandText = "DELETE FROM Menu_Items WHERE Menu_Items.Menu_Item_Id = '" + m.Menu_Item_Id1 + "'"; // setovanje SQL upita koji će se izvršiti nad bazom podataka


            return command.ExecuteNonQuery();
        }
    }
}
