﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public interface IOrderRepository
    {
        int InsertOrder(Order o);
        List<Order> SelectAllOrders();
    }
}
