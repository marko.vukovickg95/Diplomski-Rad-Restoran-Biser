﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public interface ISpotRepository
    {
        int InsertSpot(Spot s);
        List<Spot> SelectAllSpots();
        int UpdateSpot(Spot s);
        int DeleteSpot(Spot s);
    }
}
