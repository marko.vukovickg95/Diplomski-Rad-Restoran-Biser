﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
   public class OrderPOMRepository : IOrderPOMRepository
    {
        public int InsertOrderPOM(OrderPOM o)
        {
            SqlConnection dataConnection = new SqlConnection();

            dataConnection.ConnectionString = GlobalVariables.connString;
            dataConnection.Open();

            SqlCommand command = new SqlCommand(); // kreiranje komande
            command.Connection = dataConnection; //setovanje konekcije komande
            command.CommandText = "INSERT INTO OrderPOMs VALUES('" + o.Menu_Item_Id1 + "', '" + o.Amount1 + "', '" + o.Price1 + "')"; // setovanje SQL upita koji će se izvršiti nad bazom podataka

            return command.ExecuteNonQuery();
        }

        //KREIRANJE UPITA ZA ISPIS PONUDE!!!

        public List<OrderPOM> SelectAllPOMOrders()
        {
            List<OrderPOM> lista = new List<OrderPOM>();

            SqlConnection dataConnection = new SqlConnection();

            dataConnection.ConnectionString = GlobalVariables.connString;
            dataConnection.Open();

            SqlCommand command = new SqlCommand(); // kreiranje komande
            command.Connection = dataConnection; //setovanje konekcije komande
            command.CommandText = "SELECT * FROM OrderPOMs";

            SqlDataReader dataReader = command.ExecuteReader();

            while (dataReader.Read())
            {
                OrderPOM o = new OrderPOM();
                // za svaki red se uzima vrednost određene kolone (0 - prva kolona)
                o.OrderPOM_Id1 = dataReader.GetInt32(0);
                o.Menu_Item_Id1 = dataReader.GetInt32(1);
                o.Amount1 = dataReader.GetInt32(2);
                o.Price1 = dataReader.GetInt32(3);
  
                lista.Add(o); // svaki student se na kraju može ubaciti u neku listu
            }

            dataConnection.Close();
            return lista;
        }

        public int DeletePOMOrders()
        {
            SqlConnection dataConnection = new SqlConnection();

            dataConnection.ConnectionString = GlobalVariables.connString;
            dataConnection.Open();

            SqlCommand command = new SqlCommand(); // kreiranje komande
            command.Connection = dataConnection; //setovanje konekcije komande
            command.CommandText = "TRUNCATE TABLE OrderPOMs"; // setovanje SQL upita koji će se izvršiti nad bazom podataka


            return command.ExecuteNonQuery();
        }
    }
}
