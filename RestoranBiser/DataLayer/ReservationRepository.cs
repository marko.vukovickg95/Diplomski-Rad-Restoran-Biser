﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
   public class ReservationRepository : IReservationRepository
    {
        //KREIRANJE UPITA ZA UNOS PONUDE U BAZU!

        public int InsertReservation(Reservation r)
        {
            SqlConnection dataConnection = new SqlConnection();

            dataConnection.ConnectionString = GlobalVariables.connString;
            dataConnection.Open();

            SqlCommand command = new SqlCommand(); // kreiranje komande
            command.Connection = dataConnection; //setovanje konekcije komande
            command.CommandText = "INSERT INTO Reservations VALUES('" + r.Event1 + "', '" + r.Date_Of_Event1 + "', '" + r.Time_Of_Event1 + "', '" + r.Number_Of_Guests1 + "', '" + r.Cost1 + "', '" + r.Customer1 + "', '" + r.Phone_Number1 + "', '" + r.Status1 + "', '" + r.Date_Of_Payment1 + "', '" + r.Offer_Id1 + "', '" + r.Bartender_Id1 + "')"; // setovanje SQL upita koji će se izvršiti nad bazom podataka

            return command.ExecuteNonQuery();
        }

        //KREIRANJE UPITA ZA ISPIS PONUDE!!!

        public List<Reservation> SelectAllReservations()
        {
            List<Reservation> lista = new List<Reservation>();

            SqlConnection dataConnection = new SqlConnection();

            dataConnection.ConnectionString = GlobalVariables.connString;
            dataConnection.Open();

            SqlCommand command = new SqlCommand(); // kreiranje komande
            command.Connection = dataConnection; //setovanje konekcije komande
            command.CommandText = "SELECT * FROM Reservations";

            SqlDataReader dataReader = command.ExecuteReader();

            while (dataReader.Read())
            {
                Reservation r = new Reservation();
                // za svaki red se uzima vrednost određene kolone (0 - prva kolona)
                r.Reservation_Id1 = dataReader.GetInt32(0);
                r.Event1 = dataReader.GetString(1);
                r.Date_Of_Event1 = dataReader.GetDateTime(2);
                r.Time_Of_Event1 = dataReader.GetString(3);
                r.Number_Of_Guests1 = dataReader.GetInt32(4);
                r.Cost1 = dataReader.GetInt32(5);
                r.Customer1 = dataReader.GetString(6);
                r.Phone_Number1 = dataReader.GetInt32(7);
                r.Status1 = dataReader.GetString(8);
                r.Date_Of_Payment1 = dataReader.GetDateTime(9);
                r.Offer_Id1 = dataReader.GetInt32(10);
                r.Bartender_Id1 = dataReader.GetInt32(11);

                lista.Add(r); // svaki student se na kraju može ubaciti u neku listu
            }

            dataConnection.Close();
            return lista;
        }

        //UPDATE METODA ZA AZURIRANJE PONUDE
        public int UpdateReservation(Reservation r)
        {
            SqlConnection dataConnection = new SqlConnection();

            dataConnection.ConnectionString = GlobalVariables.connString;
            dataConnection.Open();

            SqlCommand command = new SqlCommand(); // kreiranje komande
            command.Connection = dataConnection; //setovanje konekcije komande
            command.CommandText = "UPDATE Reservations SET Date_Of_Payment = '" + r.Date_Of_Payment1 + "', Status = '" + r.Status1 + "' WHERE Reservation_Id = '" + r.Reservation_Id1 + "'"; // setovanje SQL upita koji će se izvršiti nad bazom podataka

            return command.ExecuteNonQuery();
            dataConnection.Close();
        }
    }
}
