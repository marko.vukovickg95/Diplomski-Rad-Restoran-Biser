﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public interface IBartenderRepository
    {
        int InsertBartender(Bartender b);
        List<Bartender> SelectAllBartenders();
        int UpdateBartender(Bartender b);
        int DeleteBartender(Bartender b);
    }
}
