﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
   public class SpotRepository : ISpotRepository
    {

        //KREIRANJE UPITA ZA UNOS PONUDE U BAZU!

        public int InsertSpot(Spot s)
        {
            SqlConnection dataConnection = new SqlConnection();

            dataConnection.ConnectionString = GlobalVariables.connString;
            dataConnection.Open();

            SqlCommand command = new SqlCommand(); // kreiranje komande
            command.Connection = dataConnection; //setovanje konekcije komande
            command.CommandText = "INSERT INTO Spots VALUES('" + s.Status1 + "')"; // setovanje SQL upita koji će se izvršiti nad bazom podataka

            return command.ExecuteNonQuery();
        }

        //KREIRANJE UPITA ZA ISPIS PONUDE!!!

        public List<Spot> SelectAllSpots()
        {
            List<Spot> lista = new List<Spot>();

            SqlConnection dataConnection = new SqlConnection();

            dataConnection.ConnectionString = GlobalVariables.connString;
            dataConnection.Open();

            SqlCommand command = new SqlCommand(); // kreiranje komande
            command.Connection = dataConnection; //setovanje konekcije komande
            command.CommandText = "SELECT * FROM Spots";

            SqlDataReader dataReader = command.ExecuteReader();

            while (dataReader.Read())
            {
                Spot s = new Spot();
                // za svaki red se uzima vrednost određene kolone (0 - prva kolona)
                s.Spot_Id1 = dataReader.GetInt32(0);
                s.Status1 = dataReader.GetString(1);
                


                lista.Add(s); // svaki student se na kraju može ubaciti u neku listu
            }

            dataConnection.Close();
            return lista;
        }

        //UPDATE METODA ZA AZURIRANJE PONUDE
        public int UpdateSpot(Spot s)
        {
            SqlConnection dataConnection = new SqlConnection();

            dataConnection.ConnectionString = GlobalVariables.connString;
            dataConnection.Open();

            SqlCommand command = new SqlCommand(); // kreiranje komande
            command.Connection = dataConnection; //setovanje konekcije komande
            command.CommandText = "UPDATE Spots SET Status = '" + s.Status1 + "' WHERE Spot_Id = '" + s.Spot_Id1 + "'"; // setovanje SQL upita koji će se izvršiti nad bazom podataka

            return command.ExecuteNonQuery();
            dataConnection.Close();
        }

        //DELETE METODA ZA BRISANJE PONUDE
        public int DeleteSpot(Spot s)
        {
            SqlConnection dataConnection = new SqlConnection();

            dataConnection.ConnectionString = GlobalVariables.connString;
            dataConnection.Open();

            SqlCommand command = new SqlCommand(); // kreiranje komande
            command.Connection = dataConnection; //setovanje konekcije komande
            command.CommandText = "DELETE FROM Spots WHERE Spots.Spot_Id = '" + s.Spot_Id1 + "'"; // setovanje SQL upita koji će se izvršiti nad bazom podataka


            return command.ExecuteNonQuery();
        }
    }
}
