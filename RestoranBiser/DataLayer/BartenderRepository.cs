﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class BartenderRepository : IBartenderRepository
    {

        //KREIRANJE UPITA ZA UNOS PONUDE U BAZU!

        public int InsertBartender(Bartender b)
        {
            SqlConnection dataConnection = new SqlConnection();

            dataConnection.ConnectionString = GlobalVariables.connString;
            dataConnection.Open();

            SqlCommand command = new SqlCommand(); // kreiranje komande
            command.Connection = dataConnection; //setovanje konekcije komande
            command.CommandText = "INSERT INTO Bartenders VALUES('" + b.Name1 + "', '" + b.Surname1 + "', '" + b.JMBG1 + "', '" + b.Card_Number1 + "', '" + b.Phone_Number1 + "', '" + b.Username1 + "', '" + b.Password1 + "')"; // setovanje SQL upita koji će se izvršiti nad bazom podataka

            return command.ExecuteNonQuery();
        }

        //KREIRANJE UPITA ZA ISPIS PONUDE!!!

        public List<Bartender> SelectAllBartenders()
        {
            List<Bartender> lista = new List<Bartender>();

            SqlConnection dataConnection = new SqlConnection();

            dataConnection.ConnectionString = GlobalVariables.connString;
            dataConnection.Open();

            SqlCommand command = new SqlCommand(); // kreiranje komande
            command.Connection = dataConnection; //setovanje konekcije komande
            command.CommandText = "SELECT * FROM Bartenders";

            SqlDataReader dataReader = command.ExecuteReader();

            while (dataReader.Read())
            {
                Bartender b = new Bartender();
                // za svaki red se uzima vrednost određene kolone (0 - prva kolona)
                b.Bartender_Id1 = dataReader.GetInt32(0);
                b.Name1 = dataReader.GetString(1);
                b.Surname1 = dataReader.GetString(2);
                b.JMBG1 = dataReader.GetInt32(3);
                b.Card_Number1 = dataReader.GetInt32(4);
                b.Phone_Number1 = dataReader.GetInt32(5);
                b.Username1 = dataReader.GetString(6);
                b.Password1 = dataReader.GetString(7);

                lista.Add(b); // svaki student se na kraju može ubaciti u neku listu
            }

            dataConnection.Close();
            return lista;
        }


        //UPDATE METODA ZA AZURIRANJE PONUDE
        public int UpdateBartender(Bartender b)
        {
            SqlConnection dataConnection = new SqlConnection();

            dataConnection.ConnectionString = GlobalVariables.connString;
            dataConnection.Open();

            SqlCommand command = new SqlCommand(); // kreiranje komande
            command.Connection = dataConnection; //setovanje konekcije komande
            command.CommandText = "UPDATE Bartenders SET Name = '" + b.Name1 + "', Surname = '" + b.Surname1 + "', JMBG = '" + b.JMBG1 + "', Card_Number = '" + b.Card_Number1 + "', Phone_Number = '" + b.Phone_Number1 + "', Username = '" + b.Username1 + "', Password = '" + b.Password1 + "' WHERE Bartender_Id = '" + b.Bartender_Id1 + "'"; // setovanje SQL upita koji će se izvršiti nad bazom podataka

            return command.ExecuteNonQuery();
            dataConnection.Close();
        }

        //DELETE METODA ZA BRISANJE PONUDE
        public int DeleteBartender(Bartender b)
        {
            SqlConnection dataConnection = new SqlConnection();

            dataConnection.ConnectionString = GlobalVariables.connString;
            dataConnection.Open();

            SqlCommand command = new SqlCommand(); // kreiranje komande
            command.Connection = dataConnection; //setovanje konekcije komande
            command.CommandText = "DELETE FROM Bartenders WHERE Bartenders.Bartender_Id = '" + b.Bartender_Id1 + "'"; // setovanje SQL upita koji će se izvršiti nad bazom podataka


            return command.ExecuteNonQuery();
        }



     

    }
}
