﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public interface IMenu_ItemRepository
    {
        int InsertMenu_Item(Menu_Item m);
        List<Menu_Item> SelectAllMenu_Items();
        int UpdateMenu_Item(Menu_Item m);
        int DeleteMenu_Item(Menu_Item m);
    }
}
