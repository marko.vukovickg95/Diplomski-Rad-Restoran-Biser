﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class OfferRepository : IOfferRepository
    {
        //KREIRANJE UPITA ZA UNOS PONUDE U BAZU!

        public int InsertOffer(Offer o)
        {
            SqlConnection dataConnection = new SqlConnection();

            dataConnection.ConnectionString = GlobalVariables.connString;
            dataConnection.Open();

            SqlCommand command = new SqlCommand(); // kreiranje komande
            command.Connection = dataConnection; //setovanje konekcije komande
            command.CommandText = "INSERT INTO Offers VALUES('" + o.Type1 + "', '" + o.Price1 + "', '" + o.Description1 + "')"; // setovanje SQL upita koji će se izvršiti nad bazom podataka

            return command.ExecuteNonQuery();
        }

        //KREIRANJE UPITA ZA ISPIS PONUDE!!!

        public List<Offer> SelectAllOffers()
        {
            List<Offer> lista = new List<Offer>();

            SqlConnection dataConnection = new SqlConnection();

            dataConnection.ConnectionString = GlobalVariables.connString;
            dataConnection.Open();

            SqlCommand command = new SqlCommand(); // kreiranje komande
            command.Connection = dataConnection; //setovanje konekcije komande
            command.CommandText = "SELECT * FROM Offers";

            SqlDataReader dataReader = command.ExecuteReader();

            while (dataReader.Read())
            {
                Offer o = new Offer();
                // za svaki red se uzima vrednost određene kolone (0 - prva kolona)
                o.Offer_Id1 = dataReader.GetInt32(0);
                o.Type1 = dataReader.GetString(1);
                o.Price1 = dataReader.GetInt32(2);
                o.Description1 = dataReader.GetString(3);
               

                lista.Add(o); // svaki student se na kraju može ubaciti u neku listu
            }

            dataConnection.Close();
            return lista;
        }

        //UPDATE METODA ZA AZURIRANJE PONUDE
        public int UpdateOffer(Offer o)
        {
            SqlConnection dataConnection = new SqlConnection();

            dataConnection.ConnectionString = GlobalVariables.connString;
            dataConnection.Open();

            SqlCommand command = new SqlCommand(); // kreiranje komande
            command.Connection = dataConnection; //setovanje konekcije komande
            command.CommandText = "UPDATE Offers SET Type = '" + o.Type1 + "', Price = '" + o.Price1 + "', Description = '" + o.Description1 + "' WHERE Offer_Id = '" + o.Offer_Id1 + "'"; // setovanje SQL upita koji će se izvršiti nad bazom podataka

            return command.ExecuteNonQuery();
            dataConnection.Close();
        }

        //DELETE METODA ZA BRISANJE PONUDE
        public int DeleteOffer(Offer o)
        {
            SqlConnection dataConnection = new SqlConnection();

            dataConnection.ConnectionString = GlobalVariables.connString;
            dataConnection.Open();

            SqlCommand command = new SqlCommand(); // kreiranje komande
            command.Connection = dataConnection; //setovanje konekcije komande
            command.CommandText = "DELETE FROM Offers WHERE Offers.Offer_Id = '" + o.Offer_Id1 + "'"; // setovanje SQL upita koji će se izvršiti nad bazom podataka


            return command.ExecuteNonQuery();
        }

    }
}
