﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
   public class OrderRepository : IOrderRepository
    {
        //KREIRANJE UPITA ZA UNOS PONUDE U BAZU!

        public int InsertOrder(Order o)
        {
            SqlConnection dataConnection = new SqlConnection();

            dataConnection.ConnectionString = GlobalVariables.connString;
            dataConnection.Open();

            SqlCommand command = new SqlCommand(); // kreiranje komande
            command.Connection = dataConnection; //setovanje konekcije komande
            command.CommandText = "INSERT INTO Orders VALUES('" + o.Amount1 + "', '" + o.Price1 + "', '" + o.Menu_Item_Id1 + "', '" + o.Bill_Id1 + "')"; // setovanje SQL upita koji će se izvršiti nad bazom podataka

            return command.ExecuteNonQuery();
        }

        //KREIRANJE UPITA ZA ISPIS PONUDE!!!

        public List<Order> SelectAllOrders()
        {
            List<Order> lista = new List<Order>();

            SqlConnection dataConnection = new SqlConnection();

            dataConnection.ConnectionString = GlobalVariables.connString;
            dataConnection.Open();

            SqlCommand command = new SqlCommand(); // kreiranje komande
            command.Connection = dataConnection; //setovanje konekcije komande
            command.CommandText = "SELECT * FROM Orders";

            SqlDataReader dataReader = command.ExecuteReader();

            while (dataReader.Read())
            {
                Order o = new Order();
                // za svaki red se uzima vrednost određene kolone (0 - prva kolona)
                o.Order_Id1 = dataReader.GetInt32(0);
                o.Amount1 = dataReader.GetInt32(1);
                o.Price1 = dataReader.GetInt32(2);
                o.Menu_Item_Id1 = dataReader.GetInt32(3);
                o.Bill_Id1 = dataReader.GetInt32(4);


                lista.Add(o); // svaki student se na kraju može ubaciti u neku listu
            }

            dataConnection.Close();
            return lista;
        }

    }
}
