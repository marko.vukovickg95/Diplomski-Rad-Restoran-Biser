﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
   public class BillRepository : IBillRepository
    {
        //KREIRANJE UPITA ZA UNOS PONUDE U BAZU!

        public int InsertBill(Bill b)
        {
            SqlConnection dataConnection = new SqlConnection();

            dataConnection.ConnectionString = GlobalVariables.connString;
            dataConnection.Open();

            SqlCommand command = new SqlCommand(); // kreiranje komande
            command.Connection = dataConnection; //setovanje konekcije komande
            command.CommandText = "INSERT INTO Bills VALUES('" + b.Date_Of_Bill1 + "', '" + b.Cost1 + "', '" + b.Status1 + "', '" + b.Bartender_Id1 + "', '" + b.Spot_Id1 + "')"; // setovanje SQL upita koji će se izvršiti nad bazom podataka

            return command.ExecuteNonQuery();
        }

        //KREIRANJE UPITA ZA ISPIS PONUDE!!!

        public List<Bill> SelectAllBills()
        {
            List<Bill> lista = new List<Bill>();

            SqlConnection dataConnection = new SqlConnection();

            dataConnection.ConnectionString = GlobalVariables.connString;
            dataConnection.Open();

            SqlCommand command = new SqlCommand(); // kreiranje komande
            command.Connection = dataConnection; //setovanje konekcije komande
            command.CommandText = "SELECT * FROM Bills";

            SqlDataReader dataReader = command.ExecuteReader();

            while (dataReader.Read())
            {
                Bill b = new Bill();
                // za svaki red se uzima vrednost određene kolone (0 - prva kolona)
                b.Bill_Id1 = dataReader.GetInt32(0);
                b.Date_Of_Bill1 = dataReader.GetDateTime(1);
                b.Cost1 = dataReader.GetInt32(2);
                b.Status1 = dataReader.GetString(3);
                b.Bartender_Id1 = dataReader.GetInt32(4);
                b.Spot_Id1 = dataReader.GetInt32(5);

                lista.Add(b); // svaki student se na kraju može ubaciti u neku listu
            }

            dataConnection.Close();
            return lista;
        }

        //UPDATE METODA ZA AZURIRANJE PONUDE
        public int UpdateBill(Bill b)
        {
            SqlConnection dataConnection = new SqlConnection();

            dataConnection.ConnectionString = GlobalVariables.connString;
            dataConnection.Open();

            SqlCommand command = new SqlCommand(); // kreiranje komande
            command.Connection = dataConnection; //setovanje konekcije komande
            command.CommandText = "UPDATE Bills SET Cost = '" + b.Cost1 + "' WHERE Bill_Id = '" + b.Bill_Id1 + "'"; // setovanje SQL upita koji će se izvršiti nad bazom podataka

            return command.ExecuteNonQuery();
            dataConnection.Close();
        }

    
    }
}
