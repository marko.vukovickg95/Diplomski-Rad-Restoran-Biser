﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestoranBiser
{
   public class KlasaStampanja
    {
   
        private string Menu_Item_Name;
        private int Amount;
        private int Price;

        public KlasaStampanja() { }

        public KlasaStampanja( string menu_Item_Name, int amount, int price)
        {
        
            Menu_Item_Name = menu_Item_Name;
            Amount = amount;
            Price = price;
        }

        
        public string Menu_Item_Name1 { get => Menu_Item_Name; set => Menu_Item_Name = value; }
        public int Amount1 { get => Amount; set => Amount = value; }
        public int Price1 { get => Price; set => Price = value; }
    }
}
