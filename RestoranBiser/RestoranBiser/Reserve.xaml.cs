﻿using BusinessLayer;
using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RestoranBiser
{
    /// <summary>
    /// Interaction logic for Reserve.xaml
    /// </summary>
    public partial class Reserve : UserControl
    {
        private ReservationBusiness reservationBusiness;
        private OfferBusiness offerBusiness;
        private BartenderBusiness bartenderBusiness;

        public Reserve(string var)
        {
            InitializeComponent();
            IReservationRepository reservationRepository = new ReservationRepository();
            this.reservationBusiness = new ReservationBusiness(reservationRepository);

            IOfferRepository offerRepository = new OfferRepository();
            this.offerBusiness = new OfferBusiness(offerRepository);


            IBartenderRepository bartenderRepository = new BartenderRepository();
            this.bartenderBusiness = new BartenderBusiness(bartenderRepository);

            HiddenBartenderId.Text = var;

            FillReservations();
            FillComboBoxOffers();
        }

        public void FillReservations()
        {
            ReservationsListBox.Items.Clear();
            List<Reservation> listReservations = reservationBusiness.SelectAllReservations();

            
            foreach (Reservation variable in listReservations)
            {


                ReservationsListBox.Items.Add(variable.Event1 + " " + variable.Date_Of_Event1.ToShortDateString() + " " + variable.Customer1 + " " + variable.Phone_Number1 + " " + variable.Status1);
            }

        }

        public void FillComboBoxOffers()
        {
            PaketComboBox.Items.Clear();
            List<Offer> listOffers = offerBusiness.SelectAllOffers();

            foreach (Offer variable in listOffers)
            {
                PaketComboBox.Items.Add(variable.Type1);

            }
        }


        public void EmptyAll()
        {
            TextBoxDogadjaj.Text = "";
            TextBoxDatumDogadjaja.Text = "";
            VremeTimePicker.Text = "";
            TextBoxBrojLjudi.Text = "";
            HiddenPaketId.Text = "";
            TextBoxCena.Text = "";
            TextBoxKlijent.Text = "";
            TextBoxBrojTelefona.Text = "";
            StatusComboBox.Text = "";
            TextBoxDatumPlacanja.Text = "";
            HiddenBartenderId.Text = "";
            HiddenReservationId.Text = "";
        }

        private void ButtonMakeReservation_Click(object sender, RoutedEventArgs e)
        {

            int numberGuest;
            int numberPhone;
            if (Int32.TryParse(TextBoxBrojLjudi.Text, out numberGuest) && Int32.TryParse(TextBoxBrojTelefona.Text, out numberPhone))
            {
                if (TextBoxDogadjaj.Text != "" && TextBoxDatumDogadjaja.Text != "" && VremeTimePicker.Text != "" && TextBoxBrojLjudi.Text != "" && PaketComboBox.Text != "" && TextBoxCena.Text != "" && TextBoxKlijent.Text != "" && TextBoxBrojTelefona.Text != "" && StatusComboBox.Text != "" && TextBoxDatumPlacanja.Text != "")
                {


                    Reservation r = new Reservation();
                    r.Event1 = TextBoxDogadjaj.Text;
                    r.Date_Of_Event1 = Convert.ToDateTime(TextBoxDatumDogadjaja.Text);
                    r.Time_Of_Event1 = VremeTimePicker.Text;
                    r.Number_Of_Guests1 = Convert.ToInt32(TextBoxBrojLjudi.Text);
                    r.Offer_Id1 = Convert.ToInt32(HiddenPaketId.Text);
                    r.Cost1 = Convert.ToInt32(TextBoxCena.Text);
                    r.Customer1 = TextBoxKlijent.Text;
                    r.Phone_Number1 = Convert.ToInt32(TextBoxBrojTelefona.Text);
                    r.Status1 = StatusComboBox.Text;
                    r.Date_Of_Payment1 = Convert.ToDateTime(TextBoxDatumPlacanja.Text);
                    r.Bartender_Id1 = Convert.ToInt32(HiddenBartenderId.Text);
                    reservationBusiness.InsertReservation(r);
                    FillReservations();

                }
                else
                {
                    MessageBox.Show("Morate da popunite sva polja!");

                }

            }
            else
            {
                MessageBox.Show("Morate da popunite polja na pravi način!");

            }


        }

        private void PaketComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string PaketName;

            PaketName = PaketComboBox.SelectedItem.ToString();

            List<Offer> listOffers = offerBusiness.SelectAllOffers().Where(o => o.Type1 == PaketName).ToList();

            Offer o1 = listOffers.First();
            HiddenPaketId.Text = Convert.ToString(o1.Offer_Id1);

            if (TextBoxBrojLjudi.Text != "") {
                int var1 = Convert.ToInt32(TextBoxBrojLjudi.Text);

                int var2 = o1.Price1;

                int var3 = var1 * var2;

                TextBoxCena.Text = Convert.ToString(var3);
            }
        }

        private void ReservationsListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string variable;
            if (ReservationsListBox.SelectedIndex != -1) { 
            variable = ReservationsListBox.SelectedItem.ToString();

            List<Reservation> listReservations = reservationBusiness.SelectAllReservations().Where(r =>  r.Event1 + " " + r.Date_Of_Event1.ToShortDateString() + " " + r.Customer1 + " " + r.Phone_Number1 + " " + r.Status1 == variable).ToList();

            Reservation reservation = listReservations.First();
            
            List<Offer> listOffers = offerBusiness.SelectAllOffers().Where(o => o.Offer_Id1 == reservation.Offer_Id1).ToList();
            Offer o1 = listOffers.First();

            TextBoxDogadjaj.Text = reservation.Event1;
            TextBoxDatumDogadjaja.Text = Convert.ToString(reservation.Date_Of_Event1);
            VremeTimePicker.Text = reservation.Time_Of_Event1;
            TextBoxBrojLjudi.Text = Convert.ToString(reservation.Number_Of_Guests1);
            HiddenPaketId.Text = Convert.ToString(reservation.Offer_Id1);
            PaketComboBox.Text = o1.Type1;
            TextBoxCena.Text = Convert.ToString(reservation.Cost1);
            TextBoxKlijent.Text = reservation.Customer1;
            TextBoxBrojTelefona.Text = Convert.ToString(reservation.Phone_Number1);
            StatusComboBox.Text = reservation.Status1;
            TextBoxDatumPlacanja.Text = Convert.ToString(reservation.Date_Of_Payment1);
            HiddenBartenderId.Text = Convert.ToString(reservation.Bartender_Id1);
            HiddenReservationId.Text = Convert.ToString(reservation.Reservation_Id1);

            }

        }

        private void ButtonResetReservation_Click(object sender, RoutedEventArgs e)
        {
            EmptyAll();
        }

        private void ButtonUpdate_Click(object sender, RoutedEventArgs e)
        {

            int numberGuest;
            int numberPhone;
            if (Int32.TryParse(TextBoxBrojLjudi.Text, out numberGuest) && Int32.TryParse(TextBoxBrojTelefona.Text, out numberPhone))
            {
                if (TextBoxDogadjaj.Text != "" && TextBoxDatumDogadjaja.Text != "" && VremeTimePicker.Text != "" && TextBoxBrojLjudi.Text != "" && PaketComboBox.Text != "" && TextBoxCena.Text != "" && TextBoxKlijent.Text != "" && TextBoxBrojTelefona.Text != "" && StatusComboBox.Text != "" && TextBoxDatumPlacanja.Text != "")
                {


                    Reservation r = new Reservation();
                    r.Status1 = StatusComboBox.Text;
                    r.Date_Of_Payment1 = DateTime.Now;
                    r.Reservation_Id1 = Convert.ToInt32(HiddenReservationId.Text);
                    reservationBusiness.UpdateReservation(r);
                    EmptyAll();
                    FillReservations();

                }
                else
                {
                    MessageBox.Show("Morate da popunite sva polja!");

                }
            }
            else
            {
                MessageBox.Show("Morate da popunite polja na pravi način!");

            }


        }

        private void StatusComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
          
        }

        private void ButtonStampaj_Click(object sender, RoutedEventArgs e)
        {
            int numberGuest;
            int numberPhone;
            if (Int32.TryParse(TextBoxBrojLjudi.Text, out numberGuest) && Int32.TryParse(TextBoxBrojTelefona.Text, out numberPhone))
            {
                if (TextBoxDogadjaj.Text != "" && TextBoxDatumDogadjaja.Text != "" && VremeTimePicker.Text != "" && TextBoxBrojLjudi.Text != "" && PaketComboBox.Text != "" && TextBoxCena.Text != "" && TextBoxKlijent.Text != "" && TextBoxBrojTelefona.Text != "" && StatusComboBox.Text != "" && TextBoxDatumPlacanja.Text != "")
                {

                    List<Reservation> listReservations = reservationBusiness.SelectAllReservations().Where(r => r.Reservation_Id1 == Convert.ToInt32(HiddenReservationId.Text)).ToList();
                    Reservation r1 = listReservations.First();


                    List<Bartender> listBartenders = bartenderBusiness.SelectAllBartenders().Where(o => o.Bartender_Id1 == r1.Bartender_Id1).ToList();
                    Bartender b1 = listBartenders.First();


                    using (Form1 forma1 = new Form1(r1,b1))
                    {
                        forma1.ShowDialog();
                    }
                }
                else
                {
                    MessageBox.Show("Morate da popunite sva polja!");

                }
            }
            else
            {
                MessageBox.Show("Morate da popunite polja na pravi način!");

            }
        }

        private void PretraziButton_Click(object sender, RoutedEventArgs e)
        {
            ReservationsListBox.Items.Clear();

            if (PretragaTextBox.Text == "")
            {

                FillReservations();
            }
            else
            {
                
             
                string polje = PretragaTextBox.Text;



                    List<Reservation> listOfReservations = reservationBusiness.SelectAllReservations().Where(m => m.Customer1.Contains(polje) || m.Customer1.ToLower().Contains(polje) || m.Customer1.ToUpper().Contains(polje) || m.Customer1.StartsWith(polje)).ToList();

                    foreach (Reservation variable in listOfReservations)
                    {


                        ReservationsListBox.Items.Add(variable.Event1 + " " + variable.Date_Of_Event1.ToShortDateString() + " " + variable.Customer1 + " " + variable.Phone_Number1 + " " + variable.Status1);
                    }
               
            }
        }
    }
}
