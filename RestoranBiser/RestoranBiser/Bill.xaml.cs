﻿using BusinessLayer;
using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RestoranBiser
{
    /// <summary>
    /// Interaction logic for Bill.xaml
    /// </summary>
    public partial class Bill : UserControl
    {
        private Menu_ItemBusiness menu_ItemBusiness;
        private OrderBusiness orderBusiness;
        private BillBusiness billBusiness;
        private BartenderBusiness bartenderBusiness;

        public Bill()
        {
            InitializeComponent();

            IMenu_ItemRepository menu_ItemRepository = new Menu_ItemRepository();
            this.menu_ItemBusiness = new Menu_ItemBusiness(menu_ItemRepository);

            IOrderRepository orderRepository = new OrderRepository();
            this.orderBusiness = new OrderBusiness(orderRepository);

            IBillRepository billRepository = new BillRepository();
            this.billBusiness = new BillBusiness(billRepository);

            IBartenderRepository bartenderRepository = new BartenderRepository();
            this.bartenderBusiness = new BartenderBusiness(bartenderRepository);

            FillBills();
        }


        public void FillBills()
        {
            RacuniListBox.Items.Clear();
            List<DataLayer.Models.Bill> listOfBills = billBusiness.SelectAllBills();

            foreach (DataLayer.Models.Bill variable in listOfBills)
            {


                RacuniListBox.Items.Add(variable.Bill_Id1 + " " + variable.Date_Of_Bill1.ToShortDateString() + " " + variable.Date_Of_Bill1.ToShortTimeString() + " " + variable.Cost1);
            }

        }

        public void EmptyBills()
        {
            NarudzineListBox.Items.Clear();
            TextBoxBrojRačuna.Text = "";
            TextBoxDatumRačuna.Text = "";
            TextBoxVremeRačuna.Text = "";
            TextBoxIznosRačuna.Text = "";
            TextBoxRadnikRačuna.Text = "";
        }


        private void NarudzineListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void RacuniListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            EmptyBills();
            string variable;
            if (RacuniListBox.SelectedIndex != -1)
            {
                variable = RacuniListBox.SelectedItem.ToString();

                List<DataLayer.Models.Bill> listBills = billBusiness.SelectAllBills().Where(r => r.Bill_Id1 + " " + r.Date_Of_Bill1.ToShortDateString() + " " + r.Date_Of_Bill1.ToShortTimeString() + " " + r.Cost1 == variable).ToList();

                DataLayer.Models.Bill bill = listBills.First();

                TextBoxBrojRačuna.Text = Convert.ToString(bill.Bill_Id1);
                TextBoxDatumRačuna.Text = Convert.ToString(bill.Date_Of_Bill1.ToShortDateString());
                TextBoxVremeRačuna.Text = Convert.ToString(bill.Date_Of_Bill1.ToShortTimeString());
                TextBoxIznosRačuna.Text = Convert.ToString(bill.Cost1);

                List<Bartender> listBartenders = bartenderBusiness.SelectAllBartenders().Where(o => o.Bartender_Id1 == bill.Bartender_Id1).ToList();
                Bartender bartenderpom = listBartenders.First();

                TextBoxRadnikRačuna.Text = bartenderpom.Name1 + " " + bartenderpom.Surname1;

                List <DataLayer.Models.Order> listOfOrders = orderBusiness.SelectAllOrders().Where(o => o.Bill_Id1 == bill.Bill_Id1).ToList();

                

                foreach (DataLayer.Models.Order order in listOfOrders)
                {
                    List<Menu_Item> mlista = menu_ItemBusiness.SelectAllMenu_Items().Where(m => m.Menu_Item_Id1 == order.Menu_Item_Id1).ToList();
                    Menu_Item mu = mlista.First();


                    NarudzineListBox.Items.Add(order.Amount1 + " " + mu.Item_Name1 + " "+ order.Price1);
                }

           }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            RacuniListBox.Items.Clear();
            int numberBill;

            if(PretragaTextBox.Text == "")
            {

                FillBills();
            }
            else { 

            if (Int32.TryParse(PretragaTextBox.Text, out numberBill))
            {

            

            List<DataLayer.Models.Bill> listOfBills = billBusiness.SelectAllBills().Where(m => m.Bill_Id1 == Convert.ToInt32(PretragaTextBox.Text)).ToList();

            foreach (DataLayer.Models.Bill variable in listOfBills)
            {


                RacuniListBox.Items.Add(variable.Bill_Id1 + " " + variable.Date_Of_Bill1.ToShortDateString() + " " + variable.Date_Of_Bill1.ToShortTimeString() + " " + variable.Cost1);
            }
            }else{

                MessageBox.Show("Parametar pretrage mora da bude broj!");
            }
            }
        }
    }
}
