﻿using BusinessLayer;
using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace RestoranBiser
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //POVEZIVANJE SA BUSINESS LAYER-OM ACTOR I MOVIE ROLE
        private BartenderBusiness bartenderBusiness;



        public MainWindow()
        {
            InitializeComponent();

            IBartenderRepository bartenderRepository = new BartenderRepository();
            this.bartenderBusiness = new BartenderBusiness(bartenderRepository);

           
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string pom1 = TextFieldUsername.Text;
            string pom2 = TextFieldPassword.Password;
            List<Bartender> listBartenders = bartenderBusiness.Login(pom1, pom2);

            if (listBartenders.Count != 0) { 
            Bartender variable = listBartenders.First();

            if(variable != null)
            {
                this.Hide();
                    string pomocna = variable.Surname1 + " " + variable.Name1;
                    int var = variable.Bartender_Id1;
                    Meni meni = new Meni(pomocna);
                meni.UserNameRight.Text = variable.Name1;
                meni.UserId.Text = Convert.ToString(variable.Bartender_Id1);
                meni.PhoneNumber.Text = Convert.ToString(variable.Phone_Number1);
                    
                    meni.ShowDialog();
                }
            else
            {
                MessageBox.Show("Uneli ste pogresne podatke!");
                TextFieldUsername.Text = "";
                TextFieldPassword.Password = "";
            }
            }
            else
            {
                MessageBox.Show("Uneli ste pogresne podatke!");
                TextFieldUsername.Text = "";
                TextFieldPassword.Password = "";
            }

        }



        private void PasswordButton_Click(object sender, RoutedEventArgs e)
        {
            if (PasswordTextBox.Text != "")
            {
                List<Bartender> listBartenders = bartenderBusiness.SelectAllBartenders().Where(b => b.Card_Number1 == Convert.ToInt32(PasswordTextBox.Text)).ToList();
                if (listBartenders.Count != 0)
                {
                    Bartender variable = listBartenders.First();

                    PasswordTextBox.Text = variable.Password1;

                }
                else
                {
                    MessageBox.Show("Uneli ste pogresne podatke!");
                    PasswordTextBox.Text = "";
                }
            }
            else
            {
                MessageBox.Show("Unesite broj licne karte!");
                PasswordTextBox.Text = "";
            }

        }

        private void IzadjiButton_Click(object sender, RoutedEventArgs e)
        {
            SifraStackPanel.Visibility = Visibility.Hidden;
            PasswordTextBox.Text = "";
        }

       
        private void LostPass_Click(object sender, RoutedEventArgs e)
        {
            SifraStackPanel.Visibility = Visibility.Visible;
            PasswordTextBox.Text = "";
        }

        private void Help_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("Chrome", Uri.EscapeDataString("E://Faks//4. Godina//Diplomski//Diplomski-Rad-Restoran-Biser//RestoranBiser//RestoranBiser//Assets//HELP//Pomocna dokumentacija.htm"));
        }
    }
}