﻿using BusinessLayer;
using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace RestoranBiser
{
    /// <summary>
    /// Interaction logic for Meni.xaml
    /// </summary>
    public partial class Meni : Window
    {
        //POVEZIVANJE SA BUSINESS LAYER-OM ACTOR I MOVIE ROLE
        private BartenderBusiness bartenderBusiness;

        public Meni(string var)
        {
            InitializeComponent();
            IBartenderRepository bartenderRepository = new BartenderRepository();
            this.bartenderBusiness = new BartenderBusiness(bartenderRepository);

            FullName.Text = var;
            pom = FullName.Text;
         //   LogedOn();
        }

                private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            MainWindow login = new MainWindow();
            login.ShowDialog();
        }

        private void ButtonCloseMenu_Click(object sender, RoutedEventArgs e)
        {
            ButtonOpenMenu.Visibility = Visibility.Visible;
            ButtonCloseMenu.Visibility = Visibility.Collapsed;
     //       LogedOn();

        }

        private void ButtonOpenMenu_Click(object sender, RoutedEventArgs e)
        {
            ButtonOpenMenu.Visibility = Visibility.Collapsed;
            ButtonCloseMenu.Visibility = Visibility.Visible;
       //     LogedOn();
        }

        private void listViewItem_Selected(object sender, RoutedEventArgs e)
        {
          
        }

        private void Grid_MouseDown_1(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            DragMove();
        }
        private void Window_Activated(object sender, EventArgs e)
        {
     //      LogedOn();

        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
      // LogedOn();

        }
        string pom;
        private void Window_Initialized(object sender, EventArgs e)
        {
         //   LogedOn();

        }
        private void ListViewMenu_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {

         
           
            int index = ListViewMenu.SelectedIndex;
           
                            MoveCursorMenu(index);
            
                        
            switch (index)
            {
                case 0:
                    GridPrimary.Children.Clear();
                 if (FullName.Text.Equals("Kojovic Aleksandar"))
                    {
                        List<Bartender> listBartenders = bartenderBusiness.SelectAllBartenders().Where(o => o.Surname1 + " " + o.Name1 == FullName.Text).ToList();
                        Bartender b1 = listBartenders.First();
                        string pomocna = Convert.ToString(b1.Bartender_Id1);
                        GridPrimary.Children.Add(new PocetnaAdmin(pomocna));
                    }
                  else if (!FullName.Text.Equals("Kojovic Aleksandar")) {
                        List<Bartender> listBartenders = bartenderBusiness.SelectAllBartenders().Where(o => o.Surname1 + " " + o.Name1 == FullName.Text).ToList();
                        Bartender b1 = listBartenders.First();
                        string pomocna = Convert.ToString(b1.Bartender_Id1);
                        GridPrimary.Children.Add(new Pocetna(pomocna));
                       
                    }
                    break;
                case 1:
                    GridPrimary.Children.Clear();
                    List<Bartender> listBartenders1 = bartenderBusiness.SelectAllBartenders().Where(o => o.Surname1 + " " + o.Name1 == FullName.Text).ToList();
                    Bartender b5 = listBartenders1.First();
                    string pomocna1 = Convert.ToString(b5.Bartender_Id1);
                    GridPrimary.Children.Add(new Order(pomocna1));
                    break;
                case 2:
                    GridPrimary.Children.Clear();
                    List<Bartender> listBartenders2 = bartenderBusiness.SelectAllBartenders().Where(o => o.Surname1 + " " + o.Name1 == FullName.Text).ToList();
                    Bartender b2 = listBartenders2.First();
                    string pomocna2 = Convert.ToString(b2.Bartender_Id1);
                    GridPrimary.Children.Add(new Reserve(pomocna2));
                    break;
                case 3:
                    GridPrimary.Children.Clear();
                    if (FullName.Text.Equals("Kojovic Aleksandar"))
                    {
                        GridPrimary.Children.Clear();
                        List<Bartender> listBartenders3 = bartenderBusiness.SelectAllBartenders().Where(o => o.Surname1 + " " + o.Name1 == FullName.Text).ToList();
                        Bartender b3 = listBartenders3.First();
                        string pomocna3 = Convert.ToString(b3.Bartender_Id1);
                        GridPrimary.Children.Add(new Radnici(pomocna3));
                    }
                    else if (!FullName.Text.Equals("Kojovic Aleksandar"))
                    {
                        List<Bartender> listBartenders = bartenderBusiness.SelectAllBartenders().Where(o => o.Surname1 + " " + o.Name1 == FullName.Text).ToList();
                        Bartender b1 = listBartenders.First();
                        string pomocna = Convert.ToString(b1.Bartender_Id1);
                        GridPrimary.Children.Add(new Hrana(pomocna));
                    }
                    break;
                case 4:
                    GridPrimary.Children.Clear();
                    if (FullName.Text.Equals("Kojovic Aleksandar"))
                    {
                        GridPrimary.Children.Add(new Ponude());
                    }
                    else if (!FullName.Text.Equals("Kojovic Aleksandar"))
                    {
                        GridPrimary.Children.Clear();
                        List<Bartender> listBartenders4 = bartenderBusiness.SelectAllBartenders().Where(o => o.Surname1 + " " + o.Name1 == FullName.Text).ToList();
                        Bartender b4 = listBartenders4.First();
                        string pomocna4 = Convert.ToString(b4.Bartender_Id1);
                        GridPrimary.Children.Add(new Order(pomocna4));
                    }

                    break;
                case 5:
                    GridPrimary.Children.Clear();

                    GridPrimary.Children.Add(new Korisnik());
                    break;

                default:
                    
                    break;
            }
        }

        private void MoveCursorMenu(int index)
        {
            TrainsitionigContentSlide.OnApplyTemplate();
            GridCursor.Margin = new Thickness(0, (100 + (60 * index)), 0, 0);
        }

        private void UserSettings_Click(object sender, RoutedEventArgs e)
        {
            GridPrimary.Children.Clear();
            List<Bartender> listBartenders5 = bartenderBusiness.SelectAllBartenders().Where(o => o.Surname1 + " " + o.Name1 == FullName.Text).ToList();
            Bartender b6 = listBartenders5.First();
            string pomocna5 = Convert.ToString(b6.Bartender_Id1);
            GridPrimary.Children.Add(new Pice(pomocna5));
        }

        

        private void button_Click_1(object sender, RoutedEventArgs e)
        {
           
            GridPrimary.Children.Clear();
            List<Bartender> listBartenders5 = bartenderBusiness.SelectAllBartenders().Where(o => o.Surname1 + " " + o.Name1 == FullName.Text).ToList();
            Bartender b6 = listBartenders5.First();
            string pomocna5 = Convert.ToString(b6.Bartender_Id1);
            GridPrimary.Children.Add(new Hrana(pomocna5));

        }

        private void FaceButton_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.facebook.com/pages/biz/Restoran-biser-309053222529130/");

        }

        private void InstaButton_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.instagram.com/explore/locations/1022633486/restoran-biser-kragujevac/?hl=en");

        }

        private void YoutubeButton_Click(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://www.youtube.com");


        }

        private void Help_Click(object sender, RoutedEventArgs e)
        {
            if (FullName.Text.Equals("Kojovic Aleksandar"))
            {
                System.Diagnostics.Process.Start("Chrome", Uri.EscapeDataString("E://Faks//4. Godina//Diplomski//Diplomski-Rad-Restoran-Biser//RestoranBiser//RestoranBiser//Assets//HELP//Pomocna dokumentacijaAdmin.htm"));

            }
            else { 
            System.Diagnostics.Process.Start("Chrome", Uri.EscapeDataString("E://Faks//4. Godina//Diplomski//Diplomski-Rad-Restoran-Biser//RestoranBiser//RestoranBiser//Assets//HELP//Pomocna dokumentacija.htm"));
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (FullName.Text.Equals("Kojovic Aleksandar"))
            {
                System.Diagnostics.Process.Start("Chrome", Uri.EscapeDataString("E://Faks//4. Godina//Diplomski//Diplomski-Rad-Restoran-Biser//RestoranBiser//RestoranBiser//Assets//HELP//Pomocna dokumentacijaAdmin.htm"));

            }
            else { 

            System.Diagnostics.Process.Start("Chrome", Uri.EscapeDataString("E://Faks//4. Godina//Diplomski//Diplomski-Rad-Restoran-Biser//RestoranBiser//RestoranBiser//Assets//HELP//Pomocna dokumentacija.htm"));
            }
        }

    }
}