﻿using DataLayer;
using System.Windows;
using Unity;

namespace RestoranBiser
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
    public UnityContainer container = new UnityContainer();
    }
}