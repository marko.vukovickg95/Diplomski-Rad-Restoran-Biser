﻿using BusinessLayer;
using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RestoranBiser
{
    /// <summary>
    /// Interaction logic for Ponude.xaml
    /// </summary>
    public partial class Ponude : UserControl
    {
        private OfferBusiness offerBusiness;
        private Menu_ItemBusiness menu_ItemBusiness;

        public Ponude()
        {
            InitializeComponent();

            IOfferRepository offerRepository = new OfferRepository();
            this.offerBusiness = new OfferBusiness(offerRepository);

            IMenu_ItemRepository menu_ItemRepository = new Menu_ItemRepository();
            this.menu_ItemBusiness = new Menu_ItemBusiness(menu_ItemRepository);

            FillOffers();
            FillMenu();
        }

        public void FillOffers()
        {
            OffersListBox.Items.Clear();
            List<Offer> listOffers = offerBusiness.SelectAllOffers();


            foreach (Offer variable in listOffers)
            {
                OffersListBox.Items.Add(variable.Type1 + " " + variable.Price1 + " " + variable.Description1);

            }
        }

        public void EmptyAllOffers()
        {
            TextBoxNazivPonude.Text = "";
            TextBoxCenaPonude.Text = "";
            TextBoxDescriptionOffer.Text = "";

            HiddenOfferId.Text = "";
        }

        public void EmptyAllMenuItems()
        {
            TextBoxNaziv.Text = "";
            TextBoxCena.Text = "";
            TextBoxKategorija.Text = "";
            TextBoxVrsta.Text = "";
            TextBoxDescription.Text = "";
            HiddenMenuItemId.Text = "";
        }

        public void FillMenu()
        {
            MenuItemssListBox.Items.Clear();
            List<Menu_Item> listMenu_Items = menu_ItemBusiness.SelectAllMenu_Items();


            foreach (Menu_Item variable in listMenu_Items)
            {
                MenuItemssListBox.Items.Add(variable.Item_Name1 + " " + variable.Price1 + ".din" + " " + variable.Item_Category1 + " " + variable.Item_Sort1);
            }
        }

        private void MenuItemssListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string variable;

            if (MenuItemssListBox.SelectedIndex != -1)
            {
                variable = MenuItemssListBox.SelectedItem.ToString();

                List<Menu_Item> listMenu_Items = menu_ItemBusiness.SelectAllMenu_Items().Where(r => r.Item_Name1 + " " + r.Price1 + ".din" + " " + r.Item_Category1 + " " + r.Item_Sort1 == variable).ToList();

                Menu_Item menu_Item = listMenu_Items.First();

                TextBoxNaziv.Text = menu_Item.Item_Name1;
                TextBoxCena.Text = Convert.ToString(menu_Item.Price1);
                TextBoxKategorija.Text = menu_Item.Item_Category1;
                TextBoxVrsta.Text = menu_Item.Item_Sort1;
                TextBoxDescription.Text = menu_Item.Description1;
                HiddenMenuItemId.Text = Convert.ToString(menu_Item.Menu_Item_Id1);


            }
        }

        private void OffersListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string variable;

            if (OffersListBox.SelectedIndex != -1)
            {
                variable = OffersListBox.SelectedItem.ToString();

                List<Offer> listOffers = offerBusiness.SelectAllOffers().Where(o => o.Type1 + " " + o.Price1 + " " + o.Description1 == variable).ToList();

                Offer offer = listOffers.First();

                TextBoxNazivPonude.Text = offer.Type1;
                TextBoxCenaPonude.Text = Convert.ToString(offer.Price1);
                TextBoxDescriptionOffer.Text = offer.Description1;

                HiddenOfferId.Text = Convert.ToString(offer.Offer_Id1);


            }
        }

        private void ButtonResetOffer_Click(object sender, RoutedEventArgs e)
        {
            EmptyAllOffers();
        }

        private void ButtonResetMenuItem_Click(object sender, RoutedEventArgs e)
        {
            EmptyAllMenuItems();
        }

        private void ButtonMakeMenuItem_Click(object sender, RoutedEventArgs e)
        {
            int numberItemPrice;

            if (Int32.TryParse(TextBoxCena.Text, out numberItemPrice))
            {
                if (TextBoxNaziv.Text != "" && TextBoxCena.Text != "" && TextBoxKategorija.Text != "" && TextBoxVrsta.Text != "" && TextBoxDescription.Text != "")
                {


                    Menu_Item b = new Menu_Item();
                    b.Item_Name1 = TextBoxNaziv.Text;
                    b.Price1 = Convert.ToInt32(TextBoxCena.Text);
                    b.Item_Category1 = TextBoxKategorija.Text;
                    b.Item_Sort1 = TextBoxVrsta.Text;
                    b.Description1 = TextBoxDescription.Text;

                    menu_ItemBusiness.InsertMenu_Item(b);


                    FillMenu();
                    EmptyAllMenuItems();
                }
                else
                {
                    MessageBox.Show("Morate da popunite sva polja!");

                }
            }
            else
            {
                MessageBox.Show("Cena mora da bude broj!");

            }


        }

        private void ButtonMakeOffer_Click(object sender, RoutedEventArgs e)
        {
            int numberUserPrice;
            
            if (Int32.TryParse(TextBoxCenaPonude.Text, out numberUserPrice))
            {

                if (TextBoxNazivPonude.Text != "" && TextBoxCenaPonude.Text != "" && TextBoxDescriptionOffer.Text != "")
                {


                    Offer b = new Offer();
                    b.Type1 = TextBoxNazivPonude.Text;
                    b.Price1 = Convert.ToInt32(TextBoxCenaPonude.Text);
                    b.Description1 = TextBoxDescriptionOffer.Text;


                    offerBusiness.InsertOffer(b);


                    FillOffers();
                    EmptyAllOffers();

                }
                else
                {
                    MessageBox.Show("Morate da popunite sva polja!");

                }
            }
            else
            {
                MessageBox.Show("Cena mora da bude broj!");

            }


        }

        private void ButtonUpdateMenuItem_Click(object sender, RoutedEventArgs e)
        {
            int numberItemPrice;

            if (Int32.TryParse(TextBoxCena.Text, out numberItemPrice))
            {

                if (TextBoxNaziv.Text != "" && TextBoxCena.Text != "" && TextBoxKategorija.Text != "" && TextBoxVrsta.Text != "" && TextBoxDescription.Text != "")
                {


                    Menu_Item b = new Menu_Item();
                    b.Item_Name1 = TextBoxNaziv.Text;
                    b.Price1 = Convert.ToInt32(TextBoxCena.Text);
                    b.Item_Category1 = TextBoxKategorija.Text;
                    b.Item_Sort1 = TextBoxVrsta.Text;
                    b.Description1 = TextBoxDescription.Text;
                    b.Menu_Item_Id1 = Convert.ToInt32(HiddenMenuItemId.Text);

                    menu_ItemBusiness.UpdateMenu_Item(b);


                    FillMenu();
                    EmptyAllMenuItems();

                }
                else
                {
                    MessageBox.Show("Morate da popunite sva polja!");

                }
            }
            else
            {
                MessageBox.Show("Cena mora da bude broj!");

            }

        }

        private void ButtonUpdateOffer_Click(object sender, RoutedEventArgs e)
        {
            int numberUserPrice;

            if (Int32.TryParse(TextBoxCenaPonude.Text, out numberUserPrice))
            {

                if (TextBoxNazivPonude.Text != "" && TextBoxCenaPonude.Text != "" && TextBoxDescriptionOffer.Text != "")
                {


                    Offer b = new Offer();
                    b.Type1 = TextBoxNazivPonude.Text;
                    b.Price1 = Convert.ToInt32(TextBoxCenaPonude.Text);
                    b.Description1 = TextBoxDescriptionOffer.Text;
                    b.Offer_Id1 = Convert.ToInt32(HiddenOfferId.Text);

                    offerBusiness.UpdateOffer(b);


                    FillOffers();
                    EmptyAllOffers();

                }
                else
                {
                    MessageBox.Show("Morate da popunite sva polja!");

                }
            }
            else
            {
                MessageBox.Show("Cena mora da bude broj!");

            }


        }
    }
}
