﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Mail;
using BusinessLayer;
using DataLayer;
using DataLayer.Models;

namespace RestoranBiser
{
    /// <summary>
    /// Interaction logic for Pice.xaml
    /// </summary>
    public partial class Pice : UserControl
    {
        private BartenderBusiness bartenderBusiness;

        public Pice(string var)
        {
            InitializeComponent();

            IBartenderRepository bartenderRepository = new BartenderRepository();
            this.bartenderBusiness = new BartenderBusiness(bartenderRepository);

            HiddenBartenderId.Text = var;
        }

        private void EmptyAll() {

            TextBoxMessageBody.Text = "";
            TextBoxEmail.Text = "";
            TextBoxSubject.Text = "";
        }

        private void ButtonPosalji_Click(object sender, RoutedEventArgs e)
        {
            if(TextBoxMessageBody.Text != "" && TextBoxEmail.Text != "" && TextBoxSubject.Text != "") { 
            try
            {
                List<Bartender> listBartenders4 = bartenderBusiness.SelectAllBartenders().Where(o => o.Bartender_Id1 == Convert.ToInt32(HiddenBartenderId.Text)).ToList();
                Bartender b4 = listBartenders4.First();
                  

                 SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                MailMessage message = new MailMessage();
                message.From = new MailAddress("marko.vukovickg95@gmail.com");
                message.To.Add("dragicvukovic@rocketmail.com");
                message.Subject = TextBoxSubject.Text;
                TextBoxMessageBody.AppendText("\n");
                TextBoxMessageBody.AppendText("\n");
                TextBoxMessageBody.AppendText("Ime posiljaoca: " + b4.Name1 +" "+ b4.Surname1);
                TextBoxMessageBody.AppendText("\n");
                TextBoxMessageBody.AppendText("Email sa kog je poslata poruka: " + TextBoxEmail.Text);
                TextBoxMessageBody.AppendText("\n");
                message.Body = TextBoxMessageBody.Text;

                client.UseDefaultCredentials = false;
                client.EnableSsl = true;

             

                client.Credentials = new System.Net.NetworkCredential("marko.vukovickg95@gmail.com", "mortalkombat1");
                client.Send(message);
                message = null;

                MessageBox.Show("Vasa poruka je poslata.", "Obavestenje");

            }
            catch (Exception)

            {
                MessageBox.Show("Doslo je do greske.", "Obavestenje");
                   
            }
            }
            else
            {
                MessageBox.Show("Popunite sva polja!.", "Obavestenje");

            }

        }

        private void ButtonOcisti_Click(object sender, RoutedEventArgs e)
        {
            EmptyAll();
        }
    }
}
