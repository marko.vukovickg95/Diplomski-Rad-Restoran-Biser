﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RestoranBiser
{
    /// <summary>
    /// Interaction logic for PocetnaAdmin.xaml
    /// </summary>
    public partial class PocetnaAdmin : UserControl
    {
        public PocetnaAdmin(string var)
        {
            InitializeComponent();
            HiddenBartenderId.Text = var;

        

        }

        private void ButtonForReservation_Click(object sender, RoutedEventArgs e)
        {
            string pomocna = HiddenBartenderId.Text;
            GridPocetnaAdmin.Children.Clear();
            GridPocetnaAdmin.Children.Add(new Reserve(pomocna));
        }

        private void WorkersButton_Click(object sender, RoutedEventArgs e)
        {
            string pomocna = HiddenBartenderId.Text;
            GridPocetnaAdmin.Children.Clear();
            GridPocetnaAdmin.Children.Add(new Radnici(pomocna));
        }

        private void OfferButton_Click(object sender, RoutedEventArgs e)
        {
            GridPocetnaAdmin.Children.Clear();
            GridPocetnaAdmin.Children.Add(new Ponude());
        }

        private void RacuniButton_Click(object sender, RoutedEventArgs e)
        {
            string pomocna = HiddenBartenderId.Text;
            GridPocetnaAdmin.Children.Clear();
            GridPocetnaAdmin.Children.Add(new Bill());
        }
    }
}
