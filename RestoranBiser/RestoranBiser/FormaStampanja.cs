﻿using BusinessLayer;
using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RestoranBiser
{
    public partial class FormaStampanja: Form
    {
        private OrderPOMBusiness orderPOMBusiness;
        private BillBusiness billBusiness;
        private BartenderBusiness bartenderBusiness;

        List<KlasaStampanja> _lista;
      //  OrderPOM _orderPOM;
        DataLayer.Models.Bill _bill;
        Bartender _bartender;
        public FormaStampanja(Bartender bartender, DataLayer.Models.Bill bill, List<KlasaStampanja> lista)
        {
            InitializeComponent();

            IOrderPOMRepository orderPOMRepository = new OrderPOMRepository();
            this.orderPOMBusiness = new OrderPOMBusiness(orderPOMRepository);


            IBillRepository billRepository = new BillRepository();
            this.billBusiness = new BillBusiness(billRepository);

            IBartenderRepository bartenderRepository = new BartenderRepository();
            this.bartenderBusiness = new BartenderBusiness(bartenderRepository);

            _bartender = bartender;
            _bill = bill;
          //  _orderPOM = orderPOM;
            _lista = lista;
        }

        private void FormaStampanja_Load(object sender, EventArgs e)
        {
            stampaniRacun1.SetDataSource(_lista);
            stampaniRacun1.SetParameterValue("datumRacuna", _bill.Date_Of_Bill1.ToShortDateString() + " " + _bill.Date_Of_Bill1.ToShortTimeString());
            stampaniRacun1.SetParameterValue("BrojRacuna", _bill.Bill_Id1);
            stampaniRacun1.SetParameterValue("ImeRadnikaRacuna", _bartender.Name1 + " " + _bartender.Surname1);

            crystalReportViewer.ReportSource = stampaniRacun1;
            crystalReportViewer.Refresh();
        }
    }
}
