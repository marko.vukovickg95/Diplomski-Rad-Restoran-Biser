﻿using BusinessLayer;
using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RestoranBiser
{
    /// <summary>
    /// Interaction logic for Radnici.xaml
    /// </summary>
    public partial class Radnici : UserControl
    {
        private BartenderBusiness bartenderBusiness;


        public Radnici(string var)
        {

            InitializeComponent();

            IBartenderRepository bartenderRepository = new BartenderRepository();
            this.bartenderBusiness = new BartenderBusiness(bartenderRepository);
            HiddenBartenderId.Text = var;
            FillBartenders();
        }

        public void FillBartenders()
        {
            WorkersListBox.Items.Clear();
            List<Bartender> listBartenders = bartenderBusiness.SelectAllBartenders();


            foreach (Bartender variable in listBartenders)
            {
                if (variable.Name1 != "Aleksandar" && variable.Surname1 != "Kojovic")
                {
                    WorkersListBox.Items.Add(variable.Name1 + " " + variable.Surname1 + " " + variable.JMBG1 + " " + variable.Card_Number1 + " " + variable.Phone_Number1 + " " + variable.Username1 + " " + variable.Password1);
                }

            }
        }



        public void EmptyAll()
        {
            TextBoxIme.Text = "";
            TextBoxPrezime.Text = "";
            TextBoxJMBG.Text = "";
            TextBoxBrojLicneKarte.Text = "";
            TextBoxBrojTelefona.Text = "";
            TextBoxUsername.Text = "";
            TextBoxPassword.Text = ""; 
        }

        private void WorkersListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string variable;

            if (WorkersListBox.SelectedIndex != -1)
            {
                variable = WorkersListBox.SelectedItem.ToString();

                List<Bartender> listBartenders = bartenderBusiness.SelectAllBartenders().Where(r => r.Name1 + " " + r.Surname1 + " " + r.JMBG1+ " " + r.Card_Number1 + " " + r.Phone_Number1 + " " + r.Username1 + " " + r.Password1 == variable).ToList();

                Bartender bartender = listBartenders.First();

                TextBoxIme.Text = bartender.Name1;
                TextBoxPrezime.Text = bartender.Surname1;
                TextBoxJMBG.Text = Convert.ToString(bartender.JMBG1);
                TextBoxBrojLicneKarte.Text = Convert.ToString(bartender.Card_Number1);
                TextBoxBrojTelefona.Text = Convert.ToString(bartender.Phone_Number1);
                TextBoxUsername.Text = bartender.Username1;
                TextBoxPassword.Text = bartender.Password1;


                HiddenBartenderId.Text = Convert.ToString(bartender.Bartender_Id1);
                

            }
        }

        private void ButtonResetWorker_Click(object sender, RoutedEventArgs e)
        {
            EmptyAll();
        }

        private void ButtonMakeWorker_Click(object sender, RoutedEventArgs e)
        {
            int numberUserCard;
            int numberPhone;
            int numberJMBG;
            if (Regex.IsMatch(TextBoxIme.Text, @"[a-zA-Z]") && Regex.IsMatch(TextBoxPrezime.Text, @"[a-zA-Z]") && Int32.TryParse(TextBoxBrojLicneKarte.Text, out numberUserCard) && Int32.TryParse(TextBoxBrojTelefona.Text, out numberPhone) && Int32.TryParse(TextBoxJMBG.Text, out numberJMBG))
            {

                if (TextBoxIme.Text != "" && TextBoxPrezime.Text != "" && TextBoxJMBG.Text != "" && TextBoxBrojLicneKarte.Text != "" && TextBoxBrojTelefona.Text != "" && TextBoxUsername.Text != "" && TextBoxPassword.Text != "")
                {


                    Bartender b = new Bartender();
                    b.Name1 = TextBoxIme.Text;
                    b.Surname1 = TextBoxPrezime.Text;
                    b.JMBG1 = Convert.ToInt32(TextBoxJMBG.Text);
                    b.Card_Number1 = Convert.ToInt32(TextBoxBrojLicneKarte.Text);
                    b.Phone_Number1 = Convert.ToInt32(TextBoxBrojTelefona.Text);
                    b.Username1 = TextBoxUsername.Text;
                    b.Password1 = TextBoxPassword.Text;
                    bartenderBusiness.InsertBartender(b);
                    FillBartenders();

                }
                else
                {
                    MessageBox.Show("Morate da popunite sva polja!");

                }
            }
            else
            {
                MessageBox.Show("Morate da popunite polja na pravi način!");

            }


        }

        private void ButtonUpdateWorker_Click(object sender, RoutedEventArgs e)
        {
            int numberUserCard;
            int numberPhone;
            int numberJMBG;
            if (Regex.IsMatch(TextBoxIme.Text, @"[a-zA-Z]") && Regex.IsMatch(TextBoxPrezime.Text, @"[a-zA-Z]") && Int32.TryParse(TextBoxBrojLicneKarte.Text, out numberUserCard) && Int32.TryParse(TextBoxBrojTelefona.Text, out numberPhone) && Int32.TryParse(TextBoxJMBG.Text, out numberJMBG))
            {

                if (TextBoxIme.Text != "" && TextBoxPrezime.Text != "" && TextBoxJMBG.Text != "" && TextBoxBrojLicneKarte.Text != "" && TextBoxBrojTelefona.Text != "" && TextBoxUsername.Text != "" && TextBoxPassword.Text != "")
                {


                    Bartender b = new Bartender();
                    b.Name1 = TextBoxIme.Text;
                    b.Surname1 = TextBoxPrezime.Text;
                    b.JMBG1 = Convert.ToInt32(TextBoxJMBG.Text);
                    b.Card_Number1 = Convert.ToInt32(TextBoxBrojLicneKarte.Text);
                    b.Phone_Number1 = Convert.ToInt32(TextBoxBrojTelefona.Text);
                    b.Username1 = TextBoxUsername.Text;
                    b.Password1 = TextBoxPassword.Text;
                    b.Bartender_Id1 = Convert.ToInt32(HiddenBartenderId.Text);
                    bartenderBusiness.UpdateBartender(b);
                    EmptyAll();
                    FillBartenders();

                }
                else
                {
                    MessageBox.Show("Morate da popunite sva polja!");

                }
            }
            else
            {
                MessageBox.Show("Morate da popunite polja na pravi način!");

            }


        }
    }
}
