﻿using BusinessLayer;
using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RestoranBiser
{
    /// <summary>
    /// Interaction logic for Order.xaml
    /// </summary>
    public partial class Order : UserControl
    {
        private Menu_ItemBusiness menu_ItemBusiness;
        private OrderBusiness orderBusiness;
        private BillBusiness billBusiness;
        private OrderPOMBusiness orderPOMBusiness;
        private BartenderBusiness bartenderBusiness;


        public Order(string pom)
        {
            InitializeComponent();

            IMenu_ItemRepository menu_ItemRepository = new Menu_ItemRepository();
            this.menu_ItemBusiness = new Menu_ItemBusiness(menu_ItemRepository);


            IOrderRepository orderRepository = new OrderRepository();
            this.orderBusiness = new OrderBusiness(orderRepository);


            IBillRepository billRepository = new BillRepository();
            this.billBusiness = new BillBusiness(billRepository);


            IOrderPOMRepository orderPOMRepository = new OrderPOMRepository();
            this.orderPOMBusiness = new OrderPOMBusiness(orderPOMRepository);

            IBartenderRepository bartenderRepository = new BartenderRepository();
            this.bartenderBusiness = new BartenderBusiness(bartenderRepository);

            HiddenIDBartender.Text = pom;

            EmptyRacun();
            FillMenu();
        }

        public void FillMenu()
        {
            ListBoxHrana.Items.Clear();
            ListBoxPice.Items.Clear();
            LisBoxDezert.Items.Clear();
            ListBoxSalata.Items.Clear();

            List<Menu_Item> listMenu_Items = menu_ItemBusiness.SelectAllMenu_Items();


            foreach (Menu_Item variable in listMenu_Items)
            {
                if(variable.Item_Sort1 == "Alkoholno")
                {
                    ListBoxPice.Items.Add(variable.Item_Name1 + " " + variable.Price1 + ".din" );
                }
                else if (variable.Item_Sort1 != "Alkoholno" && variable.Item_Category1 == "Pice")
                {
                    ListBoxBezalkoholno.Items.Add(variable.Item_Name1 + " " + variable.Price1 + ".din" );
                }
                else if(variable.Item_Category1 == "Hrana")
                {
                    ListBoxHrana.Items.Add(variable.Item_Name1 + " " + variable.Price1 + ".din"  + " " + variable.Item_Sort1);
                }
                else if (variable.Item_Category1 == "Salata")
                {
                    ListBoxSalata.Items.Add(variable.Item_Name1 + " " + variable.Price1 + ".din"  + " " + variable.Item_Sort1);
                }
                else if (variable.Item_Category1 == "Dezert")
                {
                    LisBoxDezert.Items.Add(variable.Item_Name1 + " " + variable.Price1 + ".din" + " " + variable.Item_Sort1);
                }
            }
        }

      private void EmptyRacun()
        {
            orderPOMBusiness.DeleteOrderPOM();

        }

        private void ListBoxHrana_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string variable;

            if (ListBoxHrana.SelectedIndex != -1)
            {
                variable = ListBoxHrana.SelectedItem.ToString();

                List<Menu_Item> listMenu_Items = menu_ItemBusiness.SelectAllMenu_Items().Where(o => o.Item_Name1 + " " + o.Price1 + ".din" + " " + o.Item_Sort1 == variable).ToList();

                Menu_Item menu_Item = listMenu_Items.First();

                HiddenIDItem.Text = Convert.ToString(menu_Item.Menu_Item_Id1);

                KolicinaStackPanel.Visibility = Visibility.Visible;


            }
        }

        private void ListBoxPice_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string variable;

            if (ListBoxPice.SelectedIndex != -1)
            {
                variable = ListBoxPice.SelectedItem.ToString();

                List<Menu_Item> listMenu_Items = menu_ItemBusiness.SelectAllMenu_Items().Where(o => o.Item_Name1 + " " + o.Price1 + ".din" == variable).ToList();

                Menu_Item menu_Item = listMenu_Items.First();

                HiddenIDItem.Text = Convert.ToString(menu_Item.Menu_Item_Id1);

                KolicinaStackPanel.Visibility = Visibility.Visible;


            }
        }

        private void ListBoxBezalkoholno_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string variable;

            if (ListBoxBezalkoholno.SelectedIndex != -1)
            {
                variable = ListBoxBezalkoholno.SelectedItem.ToString();

                List<Menu_Item> listMenu_Items = menu_ItemBusiness.SelectAllMenu_Items().Where(o => o.Item_Name1 + " " + o.Price1 + ".din" == variable).ToList();

                Menu_Item menu_Item = listMenu_Items.First();

                HiddenIDItem.Text = Convert.ToString(menu_Item.Menu_Item_Id1);

                KolicinaStackPanel.Visibility = Visibility.Visible;


            }
        }

        private void ListBoxSalata_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string variable;

            if (ListBoxSalata.SelectedIndex != -1)
            {
                variable = ListBoxSalata.SelectedItem.ToString();

                List<Menu_Item> listMenu_Items = menu_ItemBusiness.SelectAllMenu_Items().Where(o => o.Item_Name1 + " " + o.Price1 + ".din" + " " + o.Item_Sort1 == variable).ToList();

                Menu_Item menu_Item = listMenu_Items.First();

                HiddenIDItem.Text = Convert.ToString(menu_Item.Menu_Item_Id1);

                KolicinaStackPanel.Visibility = Visibility.Visible;

            }
        }

        private void ListBoxDezert_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string variable;

            if (LisBoxDezert.SelectedIndex != -1)
            {
                variable = LisBoxDezert.SelectedItem.ToString();

                List<Menu_Item> listMenu_Items = menu_ItemBusiness.SelectAllMenu_Items().Where(o => o.Item_Name1 + " " + o.Price1 + ".din" + " " + o.Item_Sort1 == variable).ToList();

                Menu_Item menu_Item = listMenu_Items.First();

                HiddenIDItem.Text = Convert.ToString(menu_Item.Menu_Item_Id1);

                KolicinaStackPanel.Visibility = Visibility.Visible;


            }
        }
        int UkupnaSum;

        private void KolicinaButton_Click(object sender, RoutedEventArgs e)
        {
            KolicinaStackPanel.Visibility = Visibility.Hidden;

            if (KolicinaTextBox.Text != "")
            {
                KolicinaTextBox.Text = KolicinaTextBox.Text;
                
            }
            else
            {
                KolicinaTextBox.Text = Convert.ToString(1);
            }

         

            List<Menu_Item> listMenu_Items = menu_ItemBusiness.SelectAllMenu_Items().Where(o => o.Menu_Item_Id1 == Convert.ToInt32(HiddenIDItem.Text)).ToList();

            Menu_Item menu_Item = listMenu_Items.First();
            int pom = Convert.ToInt32(KolicinaTextBox.Text);

            OrderPOM o1 = new OrderPOM();
            o1.Menu_Item_Id1 = menu_Item.Menu_Item_Id1;
            o1.Amount1 = Convert.ToInt32(KolicinaTextBox.Text);
            o1.Price1 = menu_Item.Price1 * pom;

            orderPOMBusiness.InsertOrderPOM(o1);

            Racun.Items.Add(menu_Item.Item_Name1 + " kom: " + o1.Amount1 + " cena: " + o1.Price1);
            //////////////////////////////////////////////
            int SumPom = menu_Item.Price1 * pom;
            UkupnaSum += SumPom; 
            TextBlockUkupnaCena.Text = Convert.ToString(UkupnaSum) + ".din";


            //////////////////////////////////////////////////
            
        }

        private void ButtonOtkazi_Click(object sender, RoutedEventArgs e)
        {
            Racun.Items.Clear();
            orderPOMBusiness.DeleteOrderPOM();
            UkupnaSum = 0;
        }

        private void ButtonPotvrdi_Click(object sender, RoutedEventArgs e)
        {
            List<OrderPOM> listOrderPOMs = orderPOMBusiness.SelectAllOrderPOMs();


            DataLayer.Models.Bill b = new DataLayer.Models.Bill();
            b.Date_Of_Bill1 = DateTime.Now;
            b.Spot_Id1 = 1;
            b.Status1 = "Placen";
            b.Bartender_Id1 = Convert.ToInt32(HiddenIDBartender.Text);
            b.Cost1 = UkupnaSum;

            billBusiness.InsertBill(b);

            List<DataLayer.Models.Bill> listOfBills = billBusiness.SelectAllBills();
            DataLayer.Models.Bill b1 = listOfBills.Last();

            DataLayer.Models.Order o1 = new DataLayer.Models.Order();
            
            foreach (OrderPOM o in listOrderPOMs)
            {

                o1.Amount1 = o.Amount1;
                o1.Price1 = o.Price1;
                o1.Menu_Item_Id1 = o.Menu_Item_Id1;
                o1.Bill_Id1 = b1.Bill_Id1;

                orderBusiness.InsertOrder(o1);
      
            }


            UkupnaSum = 0;
            Racun.Items.Clear();
            orderPOMBusiness.DeleteOrderPOM();
            TextBlockUkupnaCena.Text = "";
            MessageBox.Show("Uspesno ste uneli Racun!");



            //////////////////////////////////////////////////////////



            List<Bartender> listBartenders = bartenderBusiness.SelectAllBartenders().Where(o => o.Bartender_Id1 == Convert.ToInt32(HiddenIDBartender.Text)).ToList();
            Bartender bartenderpom = listBartenders.First();

            List<DataLayer.Models.Bill> listBills = billBusiness.SelectAllBills();
            DataLayer.Models.Bill billpom = listBills.Last();

            List<KlasaStampanja> listaKlasaStampanja = new List<KlasaStampanja>();

            foreach (OrderPOM o in listOrderPOMs)
            {
                KlasaStampanja k = new KlasaStampanja();
                k.Amount1 = o.Amount1;
                k.Price1 = o.Price1;

               List<Menu_Item> mlista = menu_ItemBusiness.SelectAllMenu_Items().Where(m => m.Menu_Item_Id1 == o.Menu_Item_Id1).ToList();
                Menu_Item mu = mlista.First();

                k.Menu_Item_Name1 = mu.Item_Name1;
                listaKlasaStampanja.Add(k);
            }

           

            using (FormaStampanja formastampanja = new FormaStampanja(bartenderpom, billpom, listaKlasaStampanja))
            {
                formastampanja.ShowDialog();
            }


        }
    }
}
