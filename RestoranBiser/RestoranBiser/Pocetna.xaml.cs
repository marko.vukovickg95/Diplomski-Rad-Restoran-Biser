﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RestoranBiser
{
    /// <summary>
    /// Interaction logic for Pocetna.xaml
    /// </summary>
    public partial class Pocetna : UserControl
    {
        public Pocetna(string var)
        {
            InitializeComponent();
           HiddenBartenderId.Text = var;
        }

        private void Button_Click()
        {

        }

        private void ButtonOrder_Click(object sender, RoutedEventArgs e)
        {
            string pomocna = HiddenBartenderId.Text;
            GridPocetna.Children.Clear();
            GridPocetna.Children.Add(new Order(pomocna));
        }

        private void ButtonReserve_Click(object sender, RoutedEventArgs e)
        {
            string pomocna = HiddenBartenderId.Text;
            GridPocetna.Children.Clear();
            GridPocetna.Children.Add(new Reserve(pomocna));
        }
    }
}
