﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;
using BusinessLayer;
using DataLayer;
using DataLayer.Models;

namespace RestoranBiser
{
    public partial class Form1 : Form
    {
        private ReservationBusiness reservationBusiness;
        Reservation _reserve;
        Bartender _bartender;
        public Form1(Reservation reserve, Bartender bartender)
        {
            InitializeComponent();

            IReservationRepository reservationRepository = new ReservationRepository();
            this.reservationBusiness = new ReservationBusiness(reservationRepository);

            _reserve = reserve;
            _bartender = bartender;
        }
  //      SqlConnection dataConnection = new SqlConnection("Integrated Security=true;Initial Catalog=RestoranBiser;Data Source=(localdb)\\MSSQLLocalDB"); 
  //      string imgLocation;
  //      SqlCommand cmd;

        private void button1_Click(object sender, EventArgs e)
        {
 //           OpenFileDialog dialog = new OpenFileDialog();
   //         dialog.Filter = "png files(*.png)|*.png|jpg files(*.jpg)|*.jpg";
//
 //           if(dialog.ShowDialog() == DialogResult.OK) { 

  //          imgLocation = dialog.FileName.ToString();
  //          pictureBox1.ImageLocation = imgLocation;
        //    }
        }

        private void button2_Click(object sender, EventArgs e)
        {
         //   byte[] images = null;
         //   FileStream Stream = new FileStream(imgLocation,FileMode.Open,FileAccess.Read);
           // BinaryReader brs = new BinaryReader(Stream);
           // images = brs.ReadBytes((int)Stream.Length);

    //        dataConnection.Open();
     //       string sqlQuery = "Insert into Bartenders  VALUES('" + "Proba" + "', '" + "Proba" + "', '" + 2 + "', '" + 2 + "', '" + 2 + "', '" + "Proba" + "', '" + "Proba" + "',@images)";
     //       cmd = new SqlCommand(sqlQuery, dataConnection);
  //          cmd.Parameters.Add(new SqlParameter("@images", images));

    //        int N = cmd.ExecuteNonQuery();

     //       dataConnection.Close();
      //      MessageBox.Show(N.ToString()+"Uspesno ste uneli sliku!");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
            stampanjeIzvestaj1.SetParameterValue("FieldDatumDogadjaja", _reserve.Date_Of_Event1.ToShortDateString() + " " + _reserve.Time_Of_Event1);
            stampanjeIzvestaj1.SetParameterValue("FieldBrojRezervacije", _reserve.Reservation_Id1);
            stampanjeIzvestaj1.SetParameterValue("FieldRadnik", _bartender.Name1 + " " + _bartender.Surname1);
            stampanjeIzvestaj1.SetParameterValue("FieldDogadjaj", _reserve.Event1);
            stampanjeIzvestaj1.SetParameterValue("FieldDatumRezervacije", _reserve.Date_Of_Payment1);
            stampanjeIzvestaj1.SetParameterValue("KlijentField", _reserve.Customer1);
            stampanjeIzvestaj1.SetParameterValue("BrojGostijuField", _reserve.Number_Of_Guests1);
            stampanjeIzvestaj1.SetParameterValue("FieldCena", _reserve.Cost1);
            stampanjeIzvestaj1.SetParameterValue("FieldStatus", _reserve.Status1);


            crystalReportViewer1.ReportSource = stampanjeIzvestaj1;
            crystalReportViewer1.Refresh();
        }
    }
}
