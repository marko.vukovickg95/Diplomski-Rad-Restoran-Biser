﻿using BusinessLayer;
using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RestoranBiser
{
    /// <summary>
    /// Interaction logic for Hrana.xaml
    /// </summary>
    public partial class Hrana : UserControl
    {
        private Menu_ItemBusiness menu_ItemBusiness;
        private OrderBusiness orderBusiness;
        private BillBusiness billBusiness;
        private BartenderBusiness bartenderBusiness;

        public Hrana(string var)
        {
            InitializeComponent();

            IMenu_ItemRepository menu_ItemRepository = new Menu_ItemRepository();
            this.menu_ItemBusiness = new Menu_ItemBusiness(menu_ItemRepository);


            IOrderRepository orderRepository = new OrderRepository();
            this.orderBusiness = new OrderBusiness(orderRepository);


            IBillRepository billRepository = new BillRepository();
            this.billBusiness = new BillBusiness(billRepository);

            IBartenderRepository bartenderRepository = new BartenderRepository();
            this.bartenderBusiness = new BartenderBusiness(bartenderRepository);

            HiddenBartenderId.Text = var;
            FillUser();
        }

        string imageLocation = "";

        private void FillUser()
        {
            int variable = Convert.ToInt32(HiddenBartenderId.Text);

            List<Bartender> listBartenders = bartenderBusiness.SelectAllBartenders().Where(r => r.Bartender_Id1 == variable).ToList();

            Bartender bartender = listBartenders.First();

            TextBoxImePrezime.Text = bartender.Name1 + " " + bartender.Surname1;
            TextBoxBrojTelefona.Text = Convert.ToString(bartender.Phone_Number1);
            TextBoxBrojLicneKarte.Text = Convert.ToString(bartender.Card_Number1);
            TextBoxJMBG.Text = Convert.ToString(bartender.JMBG1);
            TextBoxUsername.Text = Convert.ToString(bartender.Username1);
            TextBoxPassword.Text = Convert.ToString(bartender.Password1);
        }

      

        private void ButtonIzmena_Click(object sender, RoutedEventArgs e)
        {
            
            int numberPhone;
            if (Int32.TryParse(TextBoxBrojTelefona.Text, out numberPhone))
            {
                if (TextBoxBrojTelefona.Text != "" && TextBoxUsername.Text != "" && TextBoxPassword.Text != "")
                {
                    int variable = Convert.ToInt32(HiddenBartenderId.Text);

                    List<Bartender> listBartenders = bartenderBusiness.SelectAllBartenders().Where(r => r.Bartender_Id1 == variable).ToList();

                    Bartender bartender = listBartenders.First();

                    Bartender b = new Bartender();
                    b.Name1 = bartender.Name1;
                    b.Surname1 = bartender.Surname1;
                    b.JMBG1 = bartender.JMBG1;
                    b.Card_Number1 = bartender.Card_Number1;
                    b.Phone_Number1 = Convert.ToInt32(TextBoxBrojTelefona.Text);
                    b.Username1 = TextBoxUsername.Text;
                    b.Password1 = TextBoxPassword.Text;
                    b.Bartender_Id1 = Convert.ToInt32(HiddenBartenderId.Text);
                    bartenderBusiness.UpdateBartender(b);

                    FillUser();

                }
                else
                {
                    MessageBox.Show("Morate da popunite sva polja!");

                }
            }
            else
            {
                MessageBox.Show("Morate da popunite polja na pravi način!");

            }


        }

    
    }
}
